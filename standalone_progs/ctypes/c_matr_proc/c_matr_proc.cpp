#include "c_matr_proc.h"

// модуль разности максимума и минимума в матрице
// matr - матрица
// nrows - число строк
// ncolumns - число столбцов
double get_abs_dif_max_min(double *matr, int nrows, int ncolumns)
{
	int i;
	int j;
	double max = matr[0];
	double min = matr[0];
	double cur_item;
	double result = 0.0;
	// идём по матрице
	for(i=0; i < nrows; ++i)
		for(j=0; j < ncolumns; ++j)
		{
			cur_item = matr[i * ncolumns + j];
			// сравниваем с минимумом и максимумом
			if(cur_item > max)
				max = cur_item;
			else if(cur_item < min)
				min = cur_item;
		}
	result = fabs(max - min);
	return result;
}