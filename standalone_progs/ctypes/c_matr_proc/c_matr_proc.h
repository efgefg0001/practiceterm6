// работа с матрицей
#ifndef __CFUNCTIONS_H__
#define __CFUNCTIONS_H__ 

#ifdef __cplusplus
extern "C" {
#endif

#define CFUNCTIONS_EXPORT 1

#ifdef CFUNCTIONS_EXPORT
    #define CFUNCTIONS_API __declspec(dllexport)
#else
    #define CFUNCTIONS_API __declspec(dllimport)
#endif

#define _USE_MATH_DEFINES
#include <math.h>

// модуль разности минимума и максимума в матрице
CFUNCTIONS_API double get_abs_dif_max_min(double *matr, int nrows, int ncolumns);

#ifdef __cplusplus
}
#endif

#endif __CFUNCTIONS_H__