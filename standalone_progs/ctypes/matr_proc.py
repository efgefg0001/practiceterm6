"""
Модуль, содержащий обёртку Си-функции, написанную с использованием библиотеки ctypes.
"""
import sys

from ctypes import *


# загружаем .dll
lib_path = "./c_matr_proc/Debug/c_matr_proc.dll"
ctypes_lib = cdll.LoadLibrary(lib_path)

# ctypes-обёртка для get_abs_dif_max_min
def get_abs_dif_max_min(matr, nrows, ncolumns):
    f = ctypes_lib.get_abs_dif_max_min
    # возвращаемый тип
    f.restype = c_double
    # получаем указатель на содержимое матрицы
    addr, size = matr.buffer_info()
    double_arr = addr
    # вызываем Си-функцию
    result = f(double_arr, c_uint(nrows), c_uint(ncolumns))
    return result
