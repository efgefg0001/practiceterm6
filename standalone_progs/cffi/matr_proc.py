"""
Модуль, содержащий cffi-обёртку для си-функции
"""
from _matr_proc import ffi, lib

def get_abs_dif_max_min(matr, nrows, ncolumns):
     """
     cffi-обёртка функции get_abs_dif_max_min
     """
     # получаем указатель на содержимое матрицы
     addr, size = matr.buffer_info()
     # приводим указатель к double *
     double_arr = ffi.cast("double *", addr)
     # вызываем Си-функцию
     result = lib.get_abs_dif_max_min(double_arr, nrows, ncolumns)
     return result



