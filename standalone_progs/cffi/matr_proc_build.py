"""
Компиляция в cffi из код на Си.
"""
from cffi import *


ffi = FFI()
# заголовок Си-функции
ffi.cdef("double get_abs_dif_max_min(double *matr, int nrows, int ncolumns);")
# исходный код тела Си-функции
ffi.set_source("_matr_proc",
"""
#include "math.h"

double get_abs_dif_max_min(double *matr, int nrows, int ncolumns)
{
	int i;
	int j;
	double max = matr[0];
	double min = matr[0];
	double cur_item;
	double result = 0.0;
	for(i=0; i < nrows; ++i)
		for(j=0; j < ncolumns; ++j)
		{
			cur_item = matr[i * ncolumns + j];
			if(cur_item > max)
				max = cur_item;
			else if(cur_item < min)
				min = cur_item;
		}
	result = fabs(max - min);
	return result;
}
""")


if __name__ == "__main__":
	# компиляция
    ffi.compile()
