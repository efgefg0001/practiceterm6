"""
Осуществление ввода матрицы
"""
import array
import re


class NumOfElInRowError(Exception):
    """
    Несовпадение заданного числа столбцов и числа введённых элементов
    """
    def __init__(self, ncolumns, n_entered):
        super().__init__("Number of entered elements don't equal to number of columns: {} != {}".format(n_entered, ncolumns))


def add_to_matr(str_words, ncolumns, matr):
    """
    добавление введённой строки в матрицу
    :param str_words: введённая строка матрицы в строчном виде
    :param ncolumns: число столбцов матрицы
    :param matr: матрица
    """
    n_entered = len(str_words)
    if n_entered != ncolumns:
        raise NumOfElInRowError(ncolumns, n_entered)
    for word in str_words:
        matr.append(float(word))


def input_matrix():
    """
    ввод матрицы с консоли
    """
    nrows = 0
    ncolumns = 0
    re_whsp = re.compile(r"\s")
    matr = array.array("d", [])
    # ввод размерности матрицы
    nrows = int(input("Number of rows in matrix = "))
    ncolumns = int(input("Number of columns in matrix = "))
    # ввод матрицы построчно
    print("Enter matrix {}x{}:".format(nrows, ncolumns))
    for i in range(nrows):
        # ввели строку
        matr_row_str = input().strip()
        # разбили на слова
        matr_row_items = re_whsp.split(matr_row_str)
        # добавили строку в матрицу
        add_to_matr(matr_row_items, ncolumns, matr)
    return (matr, nrows, ncolumns)