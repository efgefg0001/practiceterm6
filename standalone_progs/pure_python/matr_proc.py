"""
Работа с матрицей
"""
import array


def get_abs_dif_max_min(matr, nrows, ncolumns):
    """
    :param matr:  матрица
    :param nrows: число строк матрицы
    :param ncolumns: число столбцов матрицы
    :return: модуль разности между минимальным и максимальным элементом матрицы
    """
    # инициализация
    max = min = matr[0]
    # проходим по элементам матрицы
    for i in range(nrows):
        incolumns = i * ncolumns
        for j in range(ncolumns):
            cur_item = matr[incolumns + j]
            # находим минимум и максимум
            if cur_item > max:
                max = cur_item
            elif cur_item < min:
                min = cur_item
    result = abs(max - min)
    return result
