"""
Тестирование работы функции, обрабатывающей матрицу
"""
import matr_proc
import inp


if __name__ == "__main__":
    try:
        # ввод матрицы
        matr, nrows, ncolumns = inp.input_matrix()
        # получение модуля максимума и минимума в матрице, вывод
        print("abs(min-max) = {}".format(matr_proc.get_abs_dif_max_min(matr, nrows, ncolumns)))
        print("max = {}, min = {}".format(min(matr), max(matr)))
    except Exception as error:
        # ошибки
        print(str(error))
