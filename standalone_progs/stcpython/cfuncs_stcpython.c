// Си-код для модуля-расширения Python

#include "Python.h"
#include "math.h"

// модуль разности максимума и минимума в матрице
// matr - матрица
// nrows - число строк
// ncolumns - число столбцов
double get_abs_dif_max_min(double *matr, int nrows, int ncolumns)
{
	// инициализация
	int i;
	int j;
	double max = matr[0];
	double min = matr[0];
	double cur_item;
	double result = 0.0;
	// проходим по матрице
	for(i=0; i < nrows; ++i)
		for(j=0; j < ncolumns; ++j)
		{
			cur_item = matr[i * ncolumns + j];
			// находим минимум и максимум
			if(cur_item > max)
				max = cur_item;
			else if(cur_item < min)
				min = cur_item;
		}
	result = fabs(max - min);
	return result;
}

// обёртка для get_abs_dif_max_min
static PyObject *cfuncs_stcpython_get_abs_dif_max_min(PyObject *self, PyObject *args)
{
	PyObject *bufobj;
	Py_buffer view;
	double result;
	int nrows, ncolumns;
	/* получаем буфер */
	if (!PyArg_ParseTuple(args, "Oii", &bufobj, &nrows, &ncolumns)) {
		return NULL;
	}

	// извлекаем информацию из буфера
	if (PyObject_GetBuffer(bufobj, &view,
		PyBUF_ANY_CONTIGUOUS | PyBUF_FORMAT) == -1) {
		return NULL;
	}
	// проверяем размерность
	if (view.ndim != 1) {
		PyErr_SetString(PyExc_TypeError, "Expected a 1-dimensional array");
		PyBuffer_Release(&view);
		return NULL;
	}

	// проверяем тип элементов
	if (strcmp(view.format,"d") != 0) {
		PyErr_SetString(PyExc_TypeError, "Expected an array of doubles");
		PyBuffer_Release(&view);
		return NULL;
	}

	// передаём буфер в Си-функцию
	result = get_abs_dif_max_min((double *)view.buf, nrows, ncolumns);

	// закончили работу с буфером
	PyBuffer_Release(&view);
	// возвращаем результат в PYthon
	return Py_BuildValue("d", result);
}

static PyMethodDef StcpythonMethods[] = {
	{"get_abs_dif_max_min", cfuncs_stcpython_get_abs_dif_max_min, METH_VARARGS, "Find difference between max and min element of matrix."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef cfuncs_stcpython = {
   PyModuleDef_HEAD_INIT,
   "cfuncs_stcpython",   /* name of module */
   NULL,//spam_doc, /* module documentation, may be NULL */
   -1,       /* size of per-interpreter state of the module,
                or -1 if the module keeps state in global variables. */
   StcpythonMethods
};

PyMODINIT_FUNC
PyInit_cfuncs_stcpython(void)
{
    PyObject *m;
    m = PyModule_Create(&cfuncs_stcpython);
    if (m == NULL)
        return NULL;
    return m;
}
