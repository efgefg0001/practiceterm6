"""
Импорт откомпилированного расширения в Python
"""
import sys

# добавление пути до папки, где находится окомпилированный код, в path
sys.path.append("build/lib.win32-3.4")
from cfuncs_stcpython import *
