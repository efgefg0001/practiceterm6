"""
Компиляция Си-кода для модуля, импортируемого в Python
"""
from distutils.core import setup, Extension

# имя модуля, исходный код
cur_module = Extension('cfuncs_stcpython',
                    sources = ['cfuncs_stcpython.c'])

# установка некоторых свойств модуля
setup (name = 'cfuncs_stcpython',
       version = '1.0',
       description = 'This is a standard method for extension of python.',
       ext_modules = [cur_module])