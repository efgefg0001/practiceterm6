"""
Запуск графического приложения
"""

import sys
from PyQt5 import QtWidgets
from tableview import TableViewEx

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = TableViewEx(None)
    window.show()
    # ожидаем завершиния приложения
    sys.exit(app.exec_())


