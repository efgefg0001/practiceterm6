#import cfuncs_stcpython_test
from past import autotranslate
autotranslate(["weave"])

"""
import testers
import CFunctions.Debug.cfuncs_stcpython as cf_stcpython


if __name__ == "__main__":
    testers_creator = testers.TestersCreator()
    ctypes_tester = testers_creator.create_tester("ctypes")
    cffi_tester = testers_creator.create_tester("cffi")
    stcpython_tester = testers_creator.create_tester("stcpython")
    python_impl_tester = testers_creator.create_tester("python_impl")
    ctypes_tester.test()
    stcpython_tester.test()
    cffi_tester.test()
    python_impl_tester.test()
"""
