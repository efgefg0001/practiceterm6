from concurrent.futures import *
import time
import math


def wait_on_future(number, count, id):
    begin = time.perf_counter()
    for i in range(count):
        res = number ** 5 * math.sin(number)
    end = time.perf_counter()
    return id, end - begin


if __name__ == "__main__":
    compl_futures = None
    t1, t2, t3, t4 = 0, 0, 0, 0
    begin = time.perf_counter()
    with ProcessPoolExecutor(max_workers=4) as executor:
        futures = [
            executor.submit(wait_on_future, 5, 10**6, 1),
            executor.submit(wait_on_future, 5, 10**6, 2),
            executor.submit(wait_on_future, 5, 10**6, 3),
            executor.submit(wait_on_future, 5, 10**5, 4),
        ]
        compl_futures = wait(futures)
#        compl_futures = as_completed(futures)
    end = time.perf_counter()
#    print("t1 = {}, t2 = {}, t3 = {}, t4 = {}".format(t1, t2, t3, t4))
#    print("compl_futures = {}".format(compl_futures.done))
#    for i, future in enumerate(compl_futures):
    for i, future in enumerate(compl_futures.done):
        print("t{} = {}".format(i+1, future.result()), end=" ")
    print()
    print("gen time = {}".format(end - begin))
