def op_pow(stack):
    """
    возведение в степень
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a ** b )


def op_mul(stack):
    """
    умножение
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a * b )


def op_div(stack):
    """
    деление
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a / b )


def op_add(stack):
    """
    сложение
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a + b )


def op_sub(stack):
    """
    вычитание
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a - b )


def op_num(stack, num):
    """
    добавление числа num в стек
    :param stack: стек, использующийся при вычислении значения выражения
    :param num: число
    """
    stack.append( num )

# операции
ops = {
 '^': op_pow,
 '*': op_mul,
 '/': op_div,
 '+': op_add,
 '-': op_sub,
 }

def get_input(inp = None):
    """
    :param inp: выражение в формате обратной польской записи
    :return: список токенов, на которые разбивается входная строка inp
    """
    if inp is None:
        inp = input('expression: ')
    tokens = inp.strip().split()
    return tokens


def compute_rpn(rpn_str):
    """
    :param rpn_str: выражение в обратной польской записи в виде строки
    :return: значение выражения в обратной польской записи rpn_str
    """
    tokens = get_input(rpn_str)
    result = rpn_calc(tokens)
    return result


def rpn_calc_1(tokens):
    """
    :param tokens: токены, на которые разбивается выражение в обратной польской записи
    :return: значение выражения в обратной польской записи
    """
    stack = []
    table = ['TOKEN,ACTION,STACK'.split(',')]
    for token in tokens:
        if token in ops:
            ops[token](stack)
        else:
            op_num(stack, eval(token))
    return stack[0]

def rpn_calc(tokens):
    stack = []
    table = ['TOKEN,ACTION,STACK'.split(',')]
    for token in tokens:
        if token in ops:
            action = 'Apply op to top of stack'
            ops[token](stack)
            table.append( (token, action, ' '.join(str(s) for s in stack)) )
        else:
            action = 'Push num onto top of stack'
            op_num(stack, eval(token))
            table.append( (token, action, ' '.join(str(s) for s in stack)) )
    return table
if __name__ == '__main__':
#    rpn = '3 4 2 * 1 5 - 2 3 ^ ^ / +'
    rpn = "50 5 * 5.5 +"
    print( 'For RPN expression: %r\n' % rpn )
    rp = rpn_calc(get_input(rpn))
    print('\n The final output value is: %r' % rpn_calc_1(get_input(rpn)))
    maxcolwidths = [max(len(y) for y in x) for x in zip(*rp)]
    row = rp[0]
    print( ' '.join('{cell:^{width}}'.format(width=width, cell=cell) for (width, cell) in zip(maxcolwidths, row)))
    for row in rp[1:]:
        print( ' '.join('{cell:<{width}}'.format(width=width, cell=cell) for (width, cell) in zip(maxcolwidths, row)))

    print('\n The final output value is: %r' % rp[-1][2])