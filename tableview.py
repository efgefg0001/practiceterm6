"""
Работа с графическим интерфейсом.
"""
import traceback

from PyQt5 import QtWidgets, QtGui, QtCore, uic

from testers import *

# результаты всех тестов
AllTestsResult = namedtuple("AllTestsResult", "stcpython cffi ctypes pure_python")

# загрузка формы из ui-файла
FormClass = uic.loadUiType('tableview.ui')

def wait_on_future(number, count, id):
    begin = time.perf_counter()
    for i in range(count):
        res = number ** 5 * math.sin(number)
    end = time.perf_counter()
    return id, end - begin


class TableViewEx(FormClass[1], FormClass[0]):
    """
    Класс для работы с графической формой
    """
    def wait_on_future(self, number, count, id):
        begin = time.perf_counter()
        for i in range(count):
            res = number ** 5 * math.sin(number)
        end = time.perf_counter()
        return id, end - begin

    def __init__(self, parent=None):
        """
        инициализация
        """
        FormClass[1].__init__(self, parent)
        self.setupUi(self)
        # установка обработчика нажатия на кнопку
        self.calcButton.clicked.connect(self.handleButton)
        # создание модели, с которой будем работать
        self.__model = self.create_model()
        # связывание модели с представлением
        self.resTableView.setModel(self.__model)
        # создание фабрики тестировщиков
        self.__testers_creator = TestersCreator()
        # создание контейнера тестировщиков
        self.__testers = self.__testers_creator.create_testers_container()

    def create_model(self):
        """
        создание модели
        """
        model = QtGui.QStandardItemModel(0, 6, self)
        # создание заголовков столбцов
        model.setHorizontalHeaderItem(0, QtGui.QStandardItem("Программа"))
        model.setHorizontalHeaderItem(1, QtGui.QStandardItem("Число повторов"))
        model.setHorizontalHeaderItem(2, QtGui.QStandardItem("Python.h"))
        model.setHorizontalHeaderItem(3, QtGui.QStandardItem("CFFI"))
        model.setHorizontalHeaderItem(4, QtGui.QStandardItem("CTypes"))
        model.setHorizontalHeaderItem(5, QtGui.QStandardItem("Чистый Python"))
        return model

    def __set_row_data(self, i, all_tests_result):
        """
        установка в i-ю строку модели-таблицы результатов тестов
        :param i: индекс строки модели
        :param all_tests_result: результаты всех тестов
        """
        pattern = "время = {:.5} c"#,\nпамять = {} байтов"
        # название программы
        self.__model.setData(self.__model.index(i, 0, QtCore.QModelIndex()),
            all_tests_result["stcpython"][i].program)
        # число повторений
        self.__model.setData(self.__model.index(i, 1, QtCore.QModelIndex()),
            all_tests_result["stcpython"][i].rep_count)
        # stcpython
        self.__model.setData(self.__model.index(i, 2, QtCore.QModelIndex()),
            pattern.format(all_tests_result["stcpython"][i].time))#, all_tests_result.stcpython[i].memory))
        # cffi
        self.__model.setData(self.__model.index(i, 3, QtCore.QModelIndex()),
            pattern.format(all_tests_result["cffi"][i].time))#, all_tests_result.cffi[i].memory))
        # ctypes
        self.__model.setData(self.__model.index(i, 4, QtCore.QModelIndex()),
             pattern.format(all_tests_result["ctypes"][i].time))#, all_tests_result.ctypes[i].memory))
        # чистый python
        self.__model.setData(self.__model.index(i, 5, QtCore.QModelIndex()),
#             "время = {:.5} с,\nпамять = {}".format(all_tests_result.pure_python[i].time, "---"))#all_tests_result.ctypes[i].memory))
            pattern.format(all_tests_result["pure_python"][i].time))

    def __fill_model(self, all_tests_result):
        """
        заполнение модели результатами тестов
        :param all_tests_result: результаты всех тестов
        """
        tests_result, count = all_tests_result
        self.__model.setRowCount(count)
        for i in range(self.__model.rowCount()):
            self.__set_row_data(i, tests_result)

    def check(self, number):
        """
        :param number: число
        :return: положительно ли число number или нет
        """
        if number <= 0:
            raise Exception("Можно вводить только натуральные числа")
        return number

    def handleButton(self):
        """
        обработчик нажатия на кнопке
        """
        try:
            # обнуление числа строк в модели
            self.__model.setRowCount(0)
            # считывание из gui введенного числа повторений, анализ на правильность исходный данных
            rep_counts = RepCounts(
                lin=self.check(int(self.linearPrLineEdit.text())),
                branch=self.check(int(self.branchPrLineEdit.text())),
                cond=self.check(int(self.condCyclesLineEdit.text())),
                num_factorial=self.check(int(self.factorialNumberCyclesLineEdit.text())),
                num_vector=self.check(int(self.vectorNumberCyclesLineEdit.text())),
                multi_matrix=self.check(int(self.matrixMultiCyclesLineEdit.text())),
                multi_mtable=self.check(int(self.mtableMultiCyclesLineEdit.text())),
                strings=self.check(int(self.stringsLineEdit.text())),
                files=self.check(int(self.filesLineEdit.text())),
                subrs=self.check(int(self.subroutinesLineEdit.text())),
                recursive=self.check(int(self.recursiveLineEdit.text())),
                structs=self.check(int(self.structsLineEdit.text())),
                compl=self.check(int(self.complexTaskLineEdit.text())),
            )
            # запуск тестов
            all_tests_result = self.__testers.test(rep_counts)
            # заполнение модели результатами тестов
            self.__fill_model(all_tests_result)
        except Exception as error:
            # обработка ошибок
            QtWidgets.QMessageBox.warning(self, "Lab1",
                                        str(error), QtWidgets.QMessageBox.Ok)
            traceback.print_exc()
