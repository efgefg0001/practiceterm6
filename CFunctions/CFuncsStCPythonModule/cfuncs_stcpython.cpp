﻿// Си-код для модуля, импортируемого в Python

#include "include.h"
#include "cfunctions.h"

// обёртка над compute_fib
static PyObject *cfuncs_stcpython_compute_fib(PyObject *self, PyObject *args)
{
	unsigned int number;
	// разбор входных аргументов, полученных из Python
	if(!PyArg_ParseTuple(args, "I", &number))
		// неправильный формат входных аргументов
		return NULL;
	else
	{
		unsigned int memory = 0;
		// непосредственный вызов compute_fib
		int result = compute_fib(number);
		// возрат рез-в в Python
		return Py_BuildValue("(iI)", result, memory);
	}
}

// обёртка get_max_len_incr_series
static PyObject *cfuncs_stcpython_get_max_len_incr_series(PyObject *self, PyObject *args)
{
	// буфер
	PyObject *bufobj;
	// представление
	Py_buffer view;
	unsigned int result;
	unsigned int count;
	/* разбор аргументов, полученных из Python*/
	if (!PyArg_ParseTuple(args, "OI", &bufobj, &count)) {
		// неправильный формат входных аргументов
		return NULL;
	}

	/* пытаемся получить информацию из буфера*/
	if (PyObject_GetBuffer(bufobj, &view,
		PyBUF_ANY_CONTIGUOUS | PyBUF_FORMAT) == -1) {
		return NULL;
	}
	// проверка размерности
	if (view.ndim != 1) {
		PyErr_SetString(PyExc_TypeError, "Expected a 1-dimensional array");
		PyBuffer_Release(&view);
		return NULL;
	}
	
	// проверка типа элементов буфера
	if (strcmp(view.format,"d") != 0) {
		PyErr_SetString(PyExc_TypeError, "Expected an array of doubles");
		PyBuffer_Release(&view);
		return NULL;
	}
	
	// передача буфера в Си-функцию
	unsigned int memory = 0;
	result = get_max_len_incr_series((double *)view.buf, count, &memory);
	
	// закончили работать с буфером
	PyBuffer_Release(&view);
	// возврат в Python
	return Py_BuildValue("(II)", result, memory);
}

// обёртка над multipl_table
static PyObject *cfuncs_stcpython_multipl_table(PyObject *self, PyObject *args)
{

	unsigned int memory = 0;
	// непосредственный вызов
	multipl_table(&memory);
	// ссылку на None увеличили на 1
	Py_INCREF(Py_None);
	// возврат в Python
	return Py_BuildValue("(OI)", Py_None, memory);
}

// обёртка над squared_x
static PyObject *cfuncs_stcpython_squared_x(PyObject *self, PyObject *args)
{
	double x;
	// получаем x из Python
	if(!PyArg_ParseTuple(args, "d", &x))
		return NULL;
	else
	{
		unsigned int memory = 0;
		// непосредственный вызов
		double result = squared_x(x, &memory);
		// возврат в Python
		return Py_BuildValue("(dI)", result, memory);
	}
}

// обёртка над sin_degrees
static PyObject *cfuncs_stcpython_sin_degrees(PyObject *self, PyObject *args)
{
	double x;
	// получаем градусы из Python
	if(!PyArg_ParseTuple(args, "d", &x))
		return NULL;
	else
	{
		unsigned int memory = 0;
		// непосредственный вызов
		double result = sin_degrees(x, &memory);
		// возврат в Python
		return Py_BuildValue("(dI)", result, memory);
	}
}

// обёртка над normal_distr_density
static PyObject *cfuncs_stcpython_normal_distr_density(PyObject *self, PyObject *args)
{
	double x, mu, sigma;
	// аргументы из Python 
	if(!PyArg_ParseTuple(args, "ddd", &x, &mu, &sigma))
		return NULL;
	else
	{
		unsigned int memory = 0;
		// непосредственный вызов
		double result = normal_distr_density(x, mu, sigma, &memory);
		// Возврат в Python
		return Py_BuildValue("(dI)", result, memory);
	}
}

// обёртка над solve_square_equation
static PyObject *cfuncs_stcpython_solve_square_equation(PyObject *self, PyObject *args)
{
	double a, b, c;
	// получение аргументов из PYthon
	if(!PyArg_ParseTuple(args, "ddd", &a, &b, &c))
		return NULL;
	else
	{
		char result[STR_SIZE];
		unsigned int memory = 0;
		// непосредственный вызов
		strcpy(result, solve_square_equation(a, b, c, &memory));
		// возврат в Python
		return Py_BuildValue("(sI)", result, memory);
	}
}

// обёртка над sum_of_infinite_series
static PyObject *cfuncs_stcpython_sum_of_infinite_series(PyObject *self, PyObject *args)
{
	double x, epsilon;
	// аргументы из PYthon
	if(!PyArg_ParseTuple(args, "dd", &x, &epsilon))
		return NULL;
	else
	{
		unsigned int memory = 0;
		// непосредственный вызов
		double result = sum_of_infinite_series(x, epsilon, &memory);
		// возврат в PYthon
		return Py_BuildValue("(dI)", result, memory); 
	}
}

// обёртка над factorial
static PyObject *cfuncs_stcpython_factorial(PyObject *self, PyObject *args)
{
	unsigned int n;
	// аргументы из PYthon
	if(!PyArg_ParseTuple(args, "I", &n))
		return NULL;
	else
	{
		unsigned int memory = 0;
		// непосредственный вызов
		double result = factorial(n, &memory);
		// возврат в Python
		return Py_BuildValue("(dI)", result, memory);
	}
}

// обёртка над get_abs_dif_max_min
static PyObject *cfuncs_stcpython_get_abs_dif_max_min(PyObject *self, PyObject *args)
{
	// буфер
	PyObject *bufobj;
	// представление
	Py_buffer view;
	double result;
	int nrows, ncolumns;
	// получаем буфер из Python аргументов
	if (!PyArg_ParseTuple(args, "Oii", &bufobj, &nrows, &ncolumns)) {
		return NULL;
	}
	
	// извлекаем информацию из буфера
	if (PyObject_GetBuffer(bufobj, &view,
		PyBUF_ANY_CONTIGUOUS | PyBUF_FORMAT) == -1) {
		return NULL;
	}
	
	// проверка размерности
	if (view.ndim != 1) {
		PyErr_SetString(PyExc_TypeError, "Expected a 1-dimensional array");
		PyBuffer_Release(&view);
		return NULL;
	}
	
	// проверка типа
	if (strcmp(view.format,"d") != 0) {
		PyErr_SetString(PyExc_TypeError, "Expected an array of doubles");
		PyBuffer_Release(&view);
		return NULL;
	}

	unsigned int memory = 0;
	// передаём буфер в Си-функцию
	result = get_abs_dif_max_min((double *)view.buf, nrows, ncolumns, &memory);

	// закончили работу  с буфером
	PyBuffer_Release(&view);
	// результаты в PYthon
	return Py_BuildValue("(dI)", result, memory);
}

// обёртка над get_first_longest_word
static PyObject *cfuncs_stcpython_get_first_longest_word(PyObject *self, PyObject *args)
{
	char *str;
	// строка из Python
	if(!PyArg_ParseTuple(args, "s", &str))
		return NULL;
	else
	{
		char first_longest_word[STR_SIZE];
		char input_str[STR_SIZE];
		// непосредственный вызов
		strcpy(input_str, str);
		unsigned int memory = 0;
		strcpy(first_longest_word, get_first_longest_word(input_str, &memory));
		// возврат результатов в PYthon
		return Py_BuildValue("(sI)", first_longest_word, memory);
	}
}

// обёртка над copy_strs
static PyObject *cfuncs_stcpython_copy_strs(PyObject *self, PyObject *args)
{
	char error_mes[STR_SIZE];
	const char *input, *output;
	unsigned int start, end;
	int nrows, ncolumns;
	// аргументы из PYthon
	if (!PyArg_ParseTuple(args, "ssII", &input, &output, &start, &end)) {
		return NULL;
	}
	unsigned int memory = 0;
	// непосредственный вызов
	int result = copy_strs(input, output, start, end, &memory);
	// обработка ошибок
	switch(result)
	{
	case ERROR_OPEN_READ:
		sprintf(error_mes, "Can't open \"%s\" for reading.", input);
		PyErr_SetString(PyExc_IOError, error_mes);
		return NULL;
		break;
	case ERROR_OPEN_WRITE:
		sprintf(error_mes, "Can't open \"%s\" for writing.", output);
		return NULL;
		PyErr_SetString(PyExc_IOError, error_mes);
	}
	// ссылка на None + 1
	Py_INCREF(Py_None);
	
	// возврат результатов в PYthon
	return Py_BuildValue("(OI)", Py_None, memory);
}

// обёртка compute_rpn
static PyObject *cfuncs_stcpython_compute_rpn(PyObject *self, PyObject *args)
{
	char *str;
	// получение строки из Python
	if(!PyArg_ParseTuple(args, "s", &str))
		return NULL;
	else
	{
		char input_str[STR_SIZE];
		strcpy(input_str, str);
		unsigned int memory = 0;
		// непосредственный вызов
		double result = compute_rpn(input_str, &memory);
		// Возврат результатов в PYthon
		return Py_BuildValue("(dI)", result, memory);
	}
}

//  структуры
// деструктов для Point 
static void del_Point(PyObject *obj) 
{
	free(PyCapsule_GetPointer(obj,"Point"));
}

// деструктор для Triangle 
static void del_Triangle(PyObject *obj)
{
	free(PyCapsule_GetPointer(obj, "Triangle"));
}

// преобразование в Point 
static Point *PyPoint_AsPoint(PyObject *obj) 
{
	return (Point *)PyCapsule_GetPointer(obj, "Point");
}

// преобразование в Triangle 
static Triangle *PyTriangle_AsTriangle(PyObject *obj)
{
	return (Triangle *)PyCapsule_GetPointer(obj, "Triangle");
}

// преобразование из Point 
static PyObject *PyPoint_FromPoint(Point *p, int must_free) 
{
	return PyCapsule_New(p, "Point", must_free ? del_Point : NULL);
}

// преобразование из Triangle 
static PyObject *PyTriangle_FromTriangle(Triangle *tr, int must_free)
{
	return PyCapsule_New(tr, "Triangle", must_free ? del_Triangle : NULL);
}

// создание нового Point 
static PyObject *cfuncs_stcpython_Point(PyObject *self, PyObject *args) 
{
	Point *p;
	double x,y;
	if (!PyArg_ParseTuple(args,"dd",&x,&y))
		return NULL;
	p = (Point *) malloc(sizeof(Point));
	p->x = x;
	p->y = y;
	return PyPoint_FromPoint(p, 1);
}

// создание нового Triangle 
static PyObject *cfuncs_stcpython_Triangle(PyObject *self, PyObject *args)
{
	Triangle *tr;
	int id;
	PyObject *py_a, *py_b, *py_c;
	Point *a, *b, *c;
	if(!PyArg_ParseTuple(args, "IOOO", &id, &py_a, &py_b, &py_c))
		return NULL;
	if(!(a = PyPoint_AsPoint(py_a)))
		return NULL;
	if(!(b = PyPoint_AsPoint(py_b)))
		return NULL;
	if(!(c = PyPoint_AsPoint(py_c)))
		return NULL;
	tr = (Triangle *)malloc(sizeof(Triangle));
	tr->id = id;
	tr->a = a;
	tr->b = b;
	tr->c = c;
	return PyTriangle_FromTriangle(tr, 1);
}

// обёртка над distance
static PyObject *cfuncs_stcpython_distance(PyObject *self, PyObject *args) 
{
	Point *p1, *p2;
	PyObject *py_p1, *py_p2;
	double result;
	// аргументы из Python
	if (!PyArg_ParseTuple(args,"OO",&py_p1, &py_p2)) {
		return NULL;
	}
	// преобразование в Point
	if (!(p1 = PyPoint_AsPoint(py_p1))) {
		return NULL;
	}
	// преобразование в Point
	if (!(p2 = PyPoint_AsPoint(py_p2))) {
		return NULL;
	}
	// непосредственный вызов
	result = distance(p1,p2);
	// возврат результатов в PYthon
	return Py_BuildValue("d", result);
}

// обёртка над tr_square
static PyObject *cfuncs_stcpython_tr_square(PyObject *self, PyObject *args)
{
	Triangle *tr;
	PyObject *py_tr;
	double result;
	// аргументы из Python 
	if(!PyArg_ParseTuple(args, "O", &py_tr))
		return NULL;
	// преобразование в Triangle
	if(!(tr = PyTriangle_AsTriangle(py_tr)))
		return NULL;
	// непосредственный вызов
	result = tr_square(tr);
	// Возврат в Python
	return Py_BuildValue("(dI)", result, 0);
}

// обёртка над tr_contains_point
static PyObject *cfuncs_stcpython_tr_contains_point(PyObject *self, PyObject *args)
{
	Triangle *tr;
	Point *point;
	PyObject *py_tr, *py_point;
	int result;
	// аргументы из PYthon
	if(!PyArg_ParseTuple(args, "OO", &py_tr, &py_point))
		return NULL;
	// преобразование в Triangle 
	if(!(tr = PyTriangle_AsTriangle(py_tr)))
		return NULL;
	// преобразование в Point
	if(!(point = PyPoint_AsPoint(py_point)))
		return NULL;
	// непосредственный вызов
	result = tr_contains_point(tr, point);
	// возврат в PYthon
	return Py_BuildValue("(iI)", result, 0);
}

// обёртка над get_substr_levlen
static PyObject *cfuncs_stcpython_get_substr_levlen(PyObject *self, PyObject *args)
{
	const char *substr, *str;
	// аргументы из Python 
	if (!PyArg_ParseTuple(args, "ss", &substr, &str)) {
		return NULL;
	}
	// непосредственный вызов
	SubStrLevLen *result = get_substr_levlen(substr, str);
	// результат
	PyObject *py_res = Py_BuildValue("(ii)", result->substr, result->levlen);
	free(result);
	// возврат
	return py_res;
}

// таблица функций-обёрток
static PyMethodDef SpamMethods[] = {
//    {"system",  spam_system, METH_VARARGS,
//     "Execute a shell command."},
	{"get_substr_levlen", cfuncs_stcpython_get_substr_levlen, METH_VARARGS, "Find substring in string and calculate Levenstein's distance."},
	{"tr_contains_point", cfuncs_stcpython_tr_contains_point, METH_VARARGS, "Does triangle contain this point."},
	{"tr_square", cfuncs_stcpython_tr_square, METH_VARARGS, "Compute square of triangle."},
	{"Triangle", cfuncs_stcpython_Triangle, METH_VARARGS, "Create a new Triangle object."},
	{"distance", cfuncs_stcpython_distance, METH_VARARGS, "Compute distance between two points."},
	{"Point", cfuncs_stcpython_Point, METH_VARARGS, "Create a new Point object."},
	{"compute_fib", cfuncs_stcpython_compute_fib, METH_VARARGS, "Compute finacci number."},
	{"get_max_len_incr_series", cfuncs_stcpython_get_max_len_incr_series, METH_VARARGS, "Find max length of increasing series in array."},
	{"multipl_table", cfuncs_stcpython_multipl_table, METH_VARARGS, "Print multiplication table."},
	{"squared_x", cfuncs_stcpython_squared_x, METH_VARARGS, "Compute x * x."},
	{"sin_degrees", cfuncs_stcpython_sin_degrees, METH_VARARGS, "Compute sinus with degrees as argument."},
	{"normal_distr_density", cfuncs_stcpython_normal_distr_density, METH_VARARGS, "Compute valuse of function of normal distribution density."},
	{"solve_square_equation", cfuncs_stcpython_solve_square_equation, METH_VARARGS, "Solve square equation."},
	{"sum_of_infinite_series", cfuncs_stcpython_sum_of_infinite_series, METH_VARARGS, "Find sum of infinite series with some epsilon."},
	{"factorial", cfuncs_stcpython_factorial, METH_VARARGS, "Compute factorial."},
	{"get_abs_dif_max_min", cfuncs_stcpython_get_abs_dif_max_min, METH_VARARGS, "Find difference between max and min element of matrix."},
	{"get_first_longest_word", cfuncs_stcpython_get_first_longest_word, METH_VARARGS, "Find first longest word."},
	{"copy_strs", cfuncs_stcpython_copy_strs, METH_VARARGS, "Copy strings from input file to output file."},
	{"compute_rpn", cfuncs_stcpython_compute_rpn, METH_VARARGS, "Compute reverse polish notation."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

// структура, хранящая информацию о модуле
static struct PyModuleDef cfuncs_stcpython = {
   PyModuleDef_HEAD_INIT,
   "cfuncs_stcpython",   /* name of module */
   NULL,//spam_doc, /* module documentation, may be NULL */
   -1,       /* size of per-interpreter state of the module,
                or -1 if the module keeps state in global variables. */
   SpamMethods
};

// инициализация модуля
PyMODINIT_FUNC
PyInit_cfuncs_stcpython(void)
{
    PyObject *m;
    m = PyModule_Create(&cfuncs_stcpython);
    if (m == NULL)
        return NULL;
    return m;
}
