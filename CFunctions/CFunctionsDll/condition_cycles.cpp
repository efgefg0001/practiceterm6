﻿// циклы с условием

#include "cfunctions.h"

// найти сумму бесконечного ряда
// x  + x/2! + x/3! + x/4! + ...
// c заданной точностью
// x - значение x
// epsilon - точность
// memory - объём оперативной памяти
double sum_of_infinite_series(double x, double epsilon, unsigned int *memory) 
{

	int i = 1;
	double cur = x;
	++i;
	double next = cur / i;
	double sum = cur;
	while(abs(next - cur) >= epsilon)
	{
		cur = next;
		sum += cur;
		++i;
		next /= i;
	}
	if(memory != NULL)
		*memory = sizeof(int) + 5*sizeof(double);
	return sum;
}