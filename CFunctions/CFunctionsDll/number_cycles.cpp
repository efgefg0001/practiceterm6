﻿// циклы с фиксированным числом итераций

#include "cfunctions.h"

// вычисление факториала натурального числа
// number - число, факториал которого нужно вычислить
// memory - объём оперативной памяти
double factorial(unsigned int number, unsigned int *memory)
{
	double fact = 1;
	int i = 1;
	for(;i<=number;++i)
		fact *= i;
	if(memory != NULL)
		*memory = sizeof(unsigned int) + sizeof(double) + sizeof(int);
	return fact;
}

// нахождение максимальной длины последовательности возрастающих
// чисел в заданном векторе
// arr - вектор действительных чисел
// memory - объём оперативной памяти
unsigned int get_max_len_incr_series(double *arr, unsigned int count, unsigned int *memory)
{
	unsigned int len_incr_series = 1;
	unsigned int max_len_incr_series = len_incr_series;
	unsigned int i;
	for(i = 1; i < count; ++i)
		if(arr[i-1] < arr[i])
			++len_incr_series;
		else if(len_incr_series > max_len_incr_series) 
		{
			max_len_incr_series = len_incr_series;
			len_incr_series = 1;
		}
	if(len_incr_series > max_len_incr_series) 
		max_len_incr_series = len_incr_series;
	if(memory != NULL)
		*memory = 0;
	return max_len_incr_series;
}