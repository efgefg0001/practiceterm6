﻿// работа со строками

#include "cfunctions.h"

// найти первой самое длинное слово в строке
// str - входная строка
// memory - объём оперативной памяти
const char *get_first_longest_word(char *str, unsigned int *memory) 
{
	// пробельные символы
	const char *whitespace = " \t\r\n\v\f";
	// указатель на первой слово
	char *word = strtok(str, whitespace);
//	char max_len_word[256];
	// инициализация
	char *max_len_word;
	int max_len = 0; 
	int word_len;
	// проходим по всем словам
	while(word != NULL)
	{
		// длина текущего слова
		word_len = strlen(word);
		// сравниваем длину текущего слова с длиной
		// самого длинного слова
		if(word_len > max_len)	
		{
			max_len = word_len;
			max_len_word = word;
//			strcpy(max_len_word, word);
		}
		word = strtok(NULL, whitespace);
	}
	if(memory != NULL)
		*memory = (strlen(whitespace) + 1 + STR_SIZE)*sizeof(char) + 2*sizeof(int) + 2*sizeof(char *);
	return max_len_word;
}