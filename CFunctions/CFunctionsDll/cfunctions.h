﻿// Библиотека функций для анализа работы
#ifndef __CFUNCTIONS_H__
#define __CFUNCTIONS_H__ 

#ifdef __cplusplus
extern "C" {
#endif

#define CFUNCTIONS_EXPORT 1

#ifdef CFUNCTIONS_EXPORT
    #define CFUNCTIONS_API __declspec(dllexport)
#else
    #define CFUNCTIONS_API __declspec(dllimport)
#endif

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const int STR_SIZE = 256;
const double EPS = 1e-10;

// linear programs

// вычисление плотности нормального распределения
CFUNCTIONS_API double normal_distr_density(double x, double mu, double sigma, unsigned int *memory=NULL);

// x*x 
CFUNCTIONS_API double squared_x(double x, unsigned int *memory=NULL);

// вычисление синуса от угла в градусах
CFUNCTIONS_API double sin_degrees(double degrees, unsigned int *memory=NULL);

// возвращает аргумент
CFUNCTIONS_API int func(int x);


// branching programs

// решение квадратного уравнения
CFUNCTIONS_API const char *solve_square_equation(double a, double b, double c, unsigned int *memory=NULL);


// cycles with conditions

// посчёт суммы бесконечного ряда с заданной точностью
CFUNCTIONS_API double sum_of_infinite_series(double x, double epsilon, unsigned int *memory=NULL);


// cycles with number of iterations

// вычисление факториала
CFUNCTIONS_API double factorial(unsigned int number, unsigned int *memory=NULL);

// нахождение максимальной длины возрастающей последовательности в векторе
CFUNCTIONS_API unsigned int get_max_len_incr_series(double *arr, unsigned int count, unsigned int *memory=NULL);

// multi-cycles 

// нахождение модуля разности максимального и минимального элемента в матрице
CFUNCTIONS_API double get_abs_dif_max_min(double *matr, int nrows, int ncolumns, unsigned int *memory=NULL);

// печать таблицы умножения
CFUNCTIONS_API void multipl_table(unsigned int *memory=NULL);


// strings

// нахождение первого самого длинного слова в строке
CFUNCTIONS_API const char *get_first_longest_word(char *str, unsigned int *memory=NULL);


// files

// размер буфера
const int BUF_SIZE = 1001;

// ошибки при работе с файлами
typedef enum {ERROR_OPEN_READ=1, ERROR_OPEN_WRITE} file_errors; 

// копирование строк из одного файла в другой
CFUNCTIONS_API int copy_strs(const char *input, const char *output, unsigned int start, unsigned int end, unsigned int *memory=NULL);


// subroutines

// глубина стека
const int MAX_D = 256;

// вычисление значения выражения в обратной польской записи
CFUNCTIONS_API double compute_rpn(char *str, unsigned int *memory=NULL);


// recursive

// нахождение n-го числа Фибоначчи
CFUNCTIONS_API int compute_fib(int number);


// structs

// структура для точки
CFUNCTIONS_API typedef struct Point {
	double x, y;
} Point;

// расстояние между двумя точками
CFUNCTIONS_API double distance(Point *p1, Point *p2);

// структура для треугольника
CFUNCTIONS_API typedef struct Triangle {
	int id;
	Point *a, *b, *c;
} Triangle;

// площадь треугольника
CFUNCTIONS_API double tr_square(Triangle *tr);

// принадлежность точки треугольнику
CFUNCTIONS_API int tr_contains_point(Triangle *tr, Point *point);


// complex task

// структура, содержащая индекс вхождения подстроки в строку, расстояние Левенштейна
CFUNCTIONS_API typedef struct SubStrLevLen {
	int substr;
	int levlen;
} SubStrLevLen;

// нахождение индекса начала вхождения подстроки в строке, расстояния Левенштейна
CFUNCTIONS_API SubStrLevLen *get_substr_levlen(const char *substr, const char *str);

// нахождение расстояния Левенштейна
CFUNCTIONS_API int get_levlen(const char *s, const char *t);

// нахождение индекса начала вхождения подстроки в строку
CFUNCTIONS_API int get_substr(const char *substr, const char *str);

#ifdef __cplusplus
}
#endif

#endif __CFUNCTIONS_H__