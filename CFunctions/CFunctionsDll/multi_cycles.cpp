﻿// вложенные циклы

#include "cfunctions.h"

// найти модуль разности между максимальным и минимальным значениями
// матрицы
// matr - матрица
// nrows - число строк
// ncolumns - число столбцов
// memory - объём оперативной памяти
double get_abs_dif_max_min(double *matr, int nrows, int ncolumns, unsigned int *memory) 
{
	int i;
	int j;
	double min = matr[0];
	double max = matr[0];
	double cur_item;
	for(i=0; i < nrows; ++i)
		for(j=0; j < ncolumns; ++j)
		{
			cur_item = matr[i * ncolumns + j];
			if(cur_item > max)
				max = cur_item;
			else if(cur_item < min)
				min = cur_item;
		}

	double result = abs(max - min);
	if(memory != NULL)
		*memory = 4*sizeof(int) + sizeof(double *) + (/*nrows*ncolumns*/ + 4)*sizeof(double);
	return result;
}

// напечатать таблицу умножения
// memory - объём оперативной памяти
void multipl_table(unsigned int *memory) 
{
	unsigned int i, nrows = 10;
	unsigned int j, ncolumns = 10;
	for(i = 1; i < nrows; ++i)
	{
		for(j = 1; j < ncolumns; ++j)
			printf("%4d ", i * j);
		printf("\n");
	}
	if(memory != NULL)
		*memory = 0;
}