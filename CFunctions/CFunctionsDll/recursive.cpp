﻿// рекурсивные подпрограммы

#include "cfunctions.h"

// вычислить n-е число Фибоначчи
// number - номер числа Фибоначчи
int compute_fib(int number)
{
	if(number == 0)
		return 0;
	else if(number == 1)
		return 1;
	else
		return compute_fib(number-1) + compute_fib(number-2);
}