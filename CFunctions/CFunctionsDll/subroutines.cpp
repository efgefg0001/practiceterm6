﻿// работа с подпрограммами

#include "cfunctions.h"

// выход из программы с сообщением
// msg - сообщение
void die(const char *msg)
{
	fprintf(stderr, "%s", msg);
	abort();
}

// затолкнуть в стек
// v - число 
// stack - стек
// depth - глубина
void push(double v, double *stack, int *depth)
{
	if (*depth >= MAX_D) die("stack overflow\n");
	stack[(*depth)++] = v;
}

// извлечи из стека
// stack - стек
// depth - глубина 
double pop(double *stack, int *depth)
{
	if (!*depth) die("stack underflow\n");
	return stack[--(*depth)];
}

// непосредственное вычисление выражения в обратной польской записи
// s - строка, содержащая выражение в обратной польской записи
// stack - стек
// depth - глубина стека
double rpn(char *s, double *stack, int *depth)
{
	// числа
	double a, b;
	// индекс
	int i;
	// e - признак ошибки перевода из строки в double
	// w - слово
	char *e, *w = " \t\n\r\f";
	
	// извлекаем из строки слова
	for (s = strtok(s, w); s; s = strtok(0, w)) {
		// переводим текущее слово в double
		a = strtod(s, &e);
		if (e > s)		
			/*printf(" :"),*/ push(a, stack, depth);
		// производим операции
#define binop(x) /*printf("%c:", *s),*/ b = pop(stack, depth), a = pop(stack, depth), push(x, stack, depth)
		else if (*s == '+')	binop(a + b);
		else if (*s == '-')	binop(a - b);
		else if (*s == '*')	binop(a * b);
		else if (*s == '/')	binop(a / b);
		else if (*s == '^')	binop(pow(a, b));
#undef binop
		else {
			// неизвестная операция
			fprintf(stderr, "'%c': ", *s);
			die("unknown oeprator\n");
		}
//		for (i = *depth; i-- || 0 * putchar('\n'); )
//			printf(" %g", stack[i]);
	}
	// ошибка во входной строке 
	if (*depth != 1) die("stack leftover\n");
	
	// вернуть значение выражения в обратной польской
	// записи
	return pop(stack, depth);
}

// вычисление выражения в обратной польской записи
// str - строчное представление выражения в обратной польской
//    записи
// memory - объём оперативной памяти
double compute_rpn(char *str, unsigned int *memory)
{
	double stack[MAX_D];
	int depth=0;
	double result = rpn(str, stack, &depth);
	if(memory != NULL)
		*memory = MAX_D*sizeof(double) + (STR_SIZE+5)*sizeof(char) + 2*sizeof(int) + 3*sizeof(double) + sizeof(char *);
	return result;
}