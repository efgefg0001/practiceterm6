﻿// ветвящиеся программы
#include "cfunctions.h"

// нахождение корней квадратного уравнения
// a, b, c - коэффициенты квадратного уравнения
// memory - объём оперативной памяти
const char *solve_square_equation(double a, double b, double c, unsigned int *memory)
{
	char str[STR_SIZE];
	double eps = EPS;
	double D;
	if(abs(a) < eps) 
		if(abs(b) < eps)
			if(abs(c) < eps)
				sprintf(str, "%s", "infinity");
			else
				sprintf(str, "%s", "no solutions");
		else
			sprintf(str, "x=%f", -c/b);
	else
	{
		D = b*b - 4*a*c;
		if(D > 0)
			sprintf(str, "x1 = %f, x2 = %f", (-b + sqrt(D))/(2*a), (-b - sqrt(D))/(2*a));
		else if(D < 0)
			sprintf(str, "%s", "no solutions");
		else
			sprintf(str, "x1 = x2 = %f", -b/(2*a));
	}
	if(memory != NULL)
		*memory = STR_SIZE * sizeof(char) + 5*sizeof(double);
	return str;
}