﻿// комплексная задача
#include "cfunctions.h"

// минимум из двух чисел
// a, b - целые числа
int min(int a, int b)
{
	if(a < b)
		return a;
	else 
		return b;
}

// нахождение длины Левенштейна по алгоритму Вагнера-Фишера
// s - строка 1
// t - строка 2
int get_levlen(const char *s, const char *t)
{
	int result = 0;
	// длины строк
	int n = strlen(s);
    int m = strlen(t);

	// размер матрицы
	int nrows = n + 1, ncols = m + 1;

	// выделение памяти под матрицу
	int **d = (int **)malloc(sizeof(int *) * nrows);
	for(int i=0; i<nrows; ++i)
		d[i] = (int *)malloc(sizeof(int)*ncols);

	// вырожденные случаи  
	if (n == 0)
		return m;
	if (m == 0)
		return n;

	// заполнение нулевого столбца и нелевой строк матрицы 
	for (int i = 0; i <= n; i++) 
		d[i][0] = i;
    for (int j = 0; j <= m; j++) 
		d[0][j] = j;

	// расчёт матрицы D
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			// если элементы строк равны, то стоимость ноль,
			// иначе 1
            int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
            // считаем минимальную стоимость перехода 
            d[i][j] = min(
				min(d[i - 1][j] + 1, d[i][j - 1] + 1),
                d[i - 1][j - 1] + cost
			);
        }
	// запоминаем полученное расстояние Левенштейна 
	result = d[n][m];

	// освобождаем память
	for(int i=0; i<nrows; ++i)
		free(d[i]);
	free(d);
	return result; 
}

// составляем вектор, содержащий переходы по несовпадению
// substr - подстрока
int *get_fail_arr(const char *substr)
{
	int substr_len = strlen(substr);
	// выделяем память под вектор
	int *fail = (int *)malloc(sizeof(int)*substr_len);
    fail[0] = -1;
	// проходим по подстроке
	for (int i = 1; i < substr_len; ++i)
    {
		int temp = fail[i - 1];
		// производим вычисления
        while (temp > 0 && substr[temp] != substr[i - 1])
			temp = fail[temp];
		fail[i] = temp + 1;
	}
	return fail;
}

// получить индекс элемента, начиная с которого подстрока начинает
// входимь в строку, рассчитанный по алгоритму Кнута-Морриса-Пратта
// substr - подстрока
// str - строка
int get_substr(const char *substr, const char *str)
{
	int substrLoc = 0, strLoc = substrLoc;
	// получаем вектор переходов по несовпадению
	int *fail = get_fail_arr(substr);
	int str_len = strlen(str), substr_len = strlen(substr);

	// находим необходимый индекс
	while (strLoc < str_len && substrLoc < substr_len)
	{
		if (substrLoc == -1 || str[strLoc] == substr[substrLoc])
		{
			++strLoc;
			++substrLoc;
		}
		else
			substrLoc = fail[substrLoc];
	}
	return substrLoc >= substr_len ? strLoc - substrLoc: -1;
}

// найти позицию начала вхождения подстроки в строку по алг.
// Кнута-Морриса-Пратта, расстояние Левенштейна между этими
// строками по алг. Вагнера-Фишера
SubStrLevLen *get_substr_levlen(const char *substr, const char *str)
{
	int substr_ind = get_substr(substr, str);
	int levlen = get_levlen(substr, str);
	SubStrLevLen *result = (SubStrLevLen *)malloc(sizeof(SubStrLevLen));
	result->levlen = levlen;
	result->substr = substr_ind;
	return result;
}