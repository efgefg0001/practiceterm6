﻿// линейные программы
#include "cfunctions.h"

// вычисление значения функции плотности нормального распределения
// x - значение x
// mu - значение математического ожидания
// sigma - значение квадратного корня из дисперсии
// memory - объём памяти 
double normal_distr_density(double x, double mu, double sigma, unsigned int *memory)
{
	double double_sq_sigma = 2 * pow(sigma, 2); 
	double sq_x_minus_mu = pow((x-mu), 2);
	double f = 1/sqrt(M_PI * double_sq_sigma) * exp(-sq_x_minus_mu/double_sq_sigma);
	if(memory != NULL)
		*memory = 6*sizeof(double);
	return f;
}

// значение x*x
// x - значение x
// memory - объём памяти
double squared_x(double x, unsigned int *memory)
{
	if(memory != NULL)
		*memory = sizeof(double);
	return x * x;
}

// значение синуса
// degrees - угол, заданный в градусах
// memory - объём памяти
double sin_degrees(double degrees, unsigned int *memory)
{
	double radians = degrees * M_PI / 180;
	double sin_val = sin(radians);
	if(memory != NULL)
		*memory = 3 * sizeof(double);
	return sin_val;
}

int func(int x)
{
	return x;
}