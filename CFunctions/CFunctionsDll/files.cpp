﻿// работа с файлами

#include "cfunctions.h"

// скопировать строки с индексами между двумя заданными
// границами из входного файла в выходной
// input - имя входного файла
// output - имя выходного файла
// start - номер начальной строки
// end - номер конечной строки
// memory - объём оперативной памяти
int copy_strs(const char *input, const char *output, unsigned int start, unsigned int end, unsigned int *memory)
{
	FILE *in = fopen(input, "r");
	if(in == NULL)
		return ERROR_OPEN_READ;
	FILE *out = fopen(output, "w");
	if(out == NULL)
	{
		fclose(in);
		return ERROR_OPEN_WRITE;
	}
	const int buf_size = BUF_SIZE;
	char buf[buf_size];
	unsigned int i = 1;
	while(fgets(buf, buf_size, in) != NULL)
	{
		if(i >= start && i <= end)
			fputs(buf, out);
		++i;
	}
	fclose(out);
	fclose(in);
	if(memory != NULL)
		*memory = 2*sizeof(const char *) + 3*sizeof(unsigned int) + sizeof(const int) + 2*sizeof(FILE *) + BUF_SIZE*sizeof(char);
	return 0;
}