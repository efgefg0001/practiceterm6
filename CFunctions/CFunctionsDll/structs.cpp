﻿// работа со структурами

#include "cfunctions.h"

// нахождение расстояния между двумя точками
// p1 - начальная точка
// p2 - конечная точка
double distance(Point *p1, Point *p2)
{
	double dx = p2->x - p1->x;
	double dy = p2->y - p1->y;
	double dist = sqrt(pow(dx, 2) + pow(dy, 2));
	return dist;
}

// нахождение площади треугольника по формуле Герона
// tr - треугольник
double tr_square(Triangle *tr)
{
	double ab = distance(tr->a, tr->b);
	double bc = distance(tr->b, tr->c);
	double ca = distance(tr->c, tr->a);
	double p = (ab + bc + ca)/2;
	double s = sqrt(p * (p - ab) * (p - bc) * (p - ca));
	return s;
}

// определение положения точки относительно прямой, заданной
// направляющим вектором, представленным в виде двух точек 
// p1 - точка, положение которой определяем
// p2 - начальная точка вектора
// p3 - конечная точка вектора
double sign(Point *p1, Point *p2, Point *p3)
{
    return (p1->x - p3->x) * (p2->y - p3->y) - (p2->x - p3->x) * (p1->y - p3->y);
}

// определение содержания точки в треугольнике
// tr - треугольник
// point - точка
int tr_contains_point(Triangle *tr, Point *point)
{
    int b1, b2, b3;

    b1 = sign(point, tr->a, tr->b) < 0;
    b2 = sign(point, tr->b, tr->c) < 0;
    b3 = sign(point, tr->c, tr->a) < 0;

    return ((b1 == b2) && (b2 == b3));
}