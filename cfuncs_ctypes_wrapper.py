"""
Способ вывзова Си-функций из Python, используя библиотеку ctypes
"""

import array
from ctypes import *

# путь к библиотеке Си-функций
lib_path = "./CFunctions/Debug/CFunctionsDll.dll"
# загрузка этой библиотеки
ctypes_lib = cdll.LoadLibrary(lib_path)

class Point(Structure):
    """
    Структура для точки
    """
    _fields_ = [
        ("x", c_double),
        ("y", c_double)
    ]


class Triangle(Structure):
    """
    Структура для треугольника
    """
    _fields_ = [
        ("id", c_int),
        ("a", POINTER(Point)),
        ("b", POINTER(Point)),
        ("c", POINTER(Point))
    ]


def tr_square(tr):
    """
    ctypes-обёртка tr_square
    """
    f = ctypes_lib.tr_square
    # установка возвращаемого типа
    f.restype = c_double
    # вызов Си-функции
    result = f(tr)
    return (result, 0)


def tr_contains_point(tr, point):
    """
    ctypes-обёртка tr_contains_point
    """
    # вызов Си-функции
    result = ctypes_lib.tr_contains_point(tr, point)
    return (result, 0)


def sin_degrees(degrees):
    """
    ctypes-обёртка для sin_degrees
    """
    f = ctypes_lib.sin_degrees
    # установка возвращаемого типа
    f.restype = c_double
    # создание указателя на c_int
    memory = c_int(0)
    p_memory = pointer(memory)
    # преобразование градусов к c_double
    degrs = c_double(degrees)
    # вызов Си-функции
    result = f(degrs, p_memory)
    return (result, memory.value)


def solve_square_equation(a, b, c):
    """
    ctypes-обёртка над solve_square_equation
    """
    f = ctypes_lib.solve_square_equation
    # возращаемый тип
    f.restype = c_char_p

    # указатель на c_int
    memory = c_int(0)
    p_memory = pointer(memory)

    # вызов Си-функции
    result = bytes.decode(f(c_double(a), c_double(b), c_double(c), p_memory))
    return (result, memory.value)


def sum_of_infinite_series(x, epsilon):
    """
    ctypes-обёртка над sum_of_infinite_series
    """
    f = ctypes_lib.sum_of_infinite_series
    # возвращаемый тип
    f.restype = c_double

    # Указатель на c_int
    memory = c_int(0)
    p_memory = pointer(memory)

    # вызов Си-функции
    result = f(c_double(x), c_double(epsilon), p_memory)
    return (result, memory.value)


def factorial(number):
    """
    ctypes-обёртка над factorial
    """
    f = ctypes_lib.factorial
    # Возвращаемый тип
    f.restype = c_double

    # указатель на c_int
    memory = c_int(0)
    p_memory = pointer(memory)

    # вызов Си-функции
    result = f(c_uint(number), p_memory)
    return (result, memory.value)


def get_abs_dif_max_min(matr, nrows, ncolumns):
    """
    ctypes-обёртка над get_abs_dif_max_min
    """
    f = ctypes_lib.get_abs_dif_max_min
    # возвращаемый тип
    f.restype = c_double

    # указатель на c_int
    memory = c_int(0)
    p_memory = pointer(memory)

    # указатель на содержимое матрицы, размер
    addr, size = matr.buffer_info()
    double_arr = addr
    # вызов Си-функции
    result = f(double_arr, c_uint(nrows), c_uint(ncolumns), p_memory)
    return (result, memory.value)


def get_first_longest_word(in_str):
    """
    ctypes-обёртка над get_first_longest_word
    """
    f = ctypes_lib.get_first_longest_word
    # Возвращаемый тип
    f.restype = c_char_p

    # указатель на c_int
    memory = c_int(0)
    p_memory = pointer(memory)

    # входная строка в байты
    input_str = str.encode(in_str)
    # вызов Си-функции
    result = bytes.decode(f(input_str, p_memory))
    return (result, memory.value)


def copy_strs(input, output, start, end):
    """
    ctypes-обёртка над copy_strs
    """
    # указатель на c_int
    memory = c_int(0)
    p_memory = pointer(memory)

    # имя входного файлы в байты
    input_bytes = str.encode(input)
    # имя выходного файла в байты
    output_bytes = str.encode(output)
    # Вызов Си-функции
    result = ctypes_lib.copy_strs(input_bytes, output_bytes, c_uint(start), c_uint(end), p_memory)
    return (result, memory.value)


def compute_rpn(in_str):
    """
    ctypes-обёртка над compute_rpn
    """
    f = ctypes_lib.compute_rpn
    # возвращаемый тип
    f.restype = c_double

    # указатель на c_int
    memory = c_int(0)
    p_memory = pointer(memory)

    # входная строка в байты
    in_str_bytes = str.encode(in_str)
    # Вызов Си-функции
    result = ctypes_lib.compute_rpn(in_str_bytes, p_memory)
    return (result, memory.value)


def compute_fib(number):
    """
    ctypes-обёртка над compute_fib
    """
    # вызов Си-функции
    result = ctypes_lib.compute_fib(c_int(number))
    return (result, 0)


def get_max_len_incr_series(arr, count):
    """
    ctypes-обёртка над get_max_len_incr_series
    """
    # указатель на c_int
    memory = c_int(0)
    p_memory = pointer(memory)

    # указатель на содержимое вектора, размер
    addr, size = arr.buffer_info()
    # вызов Си-функции
    result = ctypes_lib.get_max_len_incr_series(addr, c_uint(count), memory)
    return (result, memory.value)


def multipl_table():
    """
    ctypes-обёртка multipl_table
    """
    # указатель на c_int
    memory = c_int(0)
    p_memory = pointer(memory)

    # вызов Си-функции
    ctypes_lib.multipl_table(memory)
    return (None, memory.value)


class SubStrLevLen(Structure):
    """
    Структура, хранящая индекс начала вхождения подстроки в строку и расстояние
    Левенштейна между подстрокой и строкой.
    """
    _fields_ = [
        ("substr", c_int),
        ("levlen", c_int)
    ]

def get_substr_levlen(substr, string):
    """
    ctypes-обёртка над get_substr_levlen
    """
    f = ctypes_lib.get_substr_levlen
    # возвращаемый тип - указатель на SubStrLevLen
    f.restype = POINTER(SubStrLevLen)
    # преобразование подстроки в байты
    substr_bytes = str.encode(substr)
    # преобразование строки в байты
    string_bytes = str.encode(string)
    # вызов Си-функции
    result = ctypes_lib.get_substr_levlen(substr_bytes, string_bytes).contents
    return result.substr, result.levlen
