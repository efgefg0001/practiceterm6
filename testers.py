"""
Тестировщики для запуска анализируемых функций, создатель тестировщиков.
"""

import abc
import time
import array
import random
import math
import multiprocessing as mp
from collections import defaultdict
from concurrent.futures import *
from collections import namedtuple
from ctypes import *


# импорт модуей, функции из которых будем анализировать
import CFunctions.Debug.cfuncs_stcpython as cf_stcpython
import cfuncs_cffi_wrapper as cf_cffi
import cfuncs_ctypes_wrapper as cf_ctypes
import python_impl


def line():
    """
    нарисовать горизантальную линию в консоли
    """
    print("{}".format(100 * "-"))

class TestersCreator:
    """
    Класс для создания тестировщиков различной реализации функций
    """
    def __get_cffi_tester(self):
        """
        :return: возвращает тестировщик для Си-функций, вызов которых осуществляется из кода Питона с использованием
                библиотеки cffi
        """
        # словарь функций
        funcs = {
            "sin_degrees": cf_cffi.sin_degrees,
            "solve_square_equation": cf_cffi.solve_square_equation,
            "sum_of_infinite_series": cf_cffi.sum_of_infinite_series,
            "factorial": cf_cffi.factorial,
            "get_abs_dif_max_min": cf_cffi.get_abs_dif_max_min,
            "get_first_longest_word": cf_cffi.get_first_longest_word,
            "copy_strs": cf_cffi.copy_strs,
            "compute_rpn": cf_cffi.compute_rpn,
            "compute_fib": cf_cffi.compute_fib,
            "get_max_len_incr_series": cf_cffi.get_max_len_incr_series,
            "multipl_table": cf_cffi.multipl_table,
            "tr_square": cf_cffi.tr_square,
            "tr_contains_point": cf_cffi.tr_contains_point,
            "get_substr_levlen": cf_cffi.get_substr_levlen,
        }
        # установка данных для некоторых анализируемых функций
        if not cf_cffi.include_convert:
            a = cf_cffi.ffi.new("Point *")
            a.x, a.y = 0, 0
            b = cf_cffi.ffi.new("Point *")
            b.x, b.y = (5, 0)
            c = cf_cffi.ffi.new("Point *")
            c.x, c.y = (0, 5)
            id = 1
            tr = cf_cffi.ffi.new("Triangle *")
            tr.id, tr.a, tr.b, tr.c = (id, a, b, c)
            point = cf_cffi.ffi.new("Point *")
            point.x, point.y = (-0.001, -0.001)
            input_data = {
                "tr": tr,
                "point": point,
                "a": a,
                "b": b,
                "c": c,
            }
        else:
            a = python_impl.Point(0, 0)
            b = python_impl.Point(5, 0)
            c = python_impl.Point(0, 5)
            id = 1
            tr = python_impl.Triangle(id, a, b, c)
            point = python_impl.Point(-0.001, -0.001)
            input_data = {
                "tr": tr,
                "point": point,
            }
        # имя анализируемого способа
        name = "cffi"
        return ModuleTester(name, funcs, input_data)

    def __get_ctypes_tester(self):
        """
        :return: возвращает тестировщик для Си-функций, вызов которых осуществляется из кода Питона с использованием
                библиотеки ctypes
        """
        # словарь анализируемых функций
        funcs = {
            "sin_degrees": cf_ctypes.sin_degrees,
            "solve_square_equation": cf_ctypes.solve_square_equation,
            "sum_of_infinite_series": cf_ctypes.sum_of_infinite_series,
            "factorial": cf_ctypes.factorial,
            "get_abs_dif_max_min": cf_ctypes.get_abs_dif_max_min,
            "get_first_longest_word": cf_ctypes.get_first_longest_word,
            "copy_strs": cf_ctypes.copy_strs,
            "compute_rpn": cf_ctypes.compute_rpn,
            "compute_fib": cf_ctypes.compute_fib,
            "get_max_len_incr_series": cf_ctypes.get_max_len_incr_series,
            "multipl_table": cf_ctypes.multipl_table,
            "tr_square": cf_ctypes.tr_square,
            "tr_contains_point": cf_ctypes.tr_contains_point,
            "get_substr_levlen": cf_ctypes.get_substr_levlen,
        }
        # установка данных для некоторых функций
        a = cf_ctypes.Point(0, 0)
        b = cf_ctypes.Point(5, 0)
        c = cf_ctypes.Point(0, 5)
        id = 1
        tr= cf_ctypes.Triangle(id, pointer(a), pointer(b), pointer(c))
        point = cf_ctypes.Point(-0.001, -0.001)
        input_data = {
            "tr": pointer(tr),
            "point": pointer(point),
        }
        # имя способа
        name = "ctypes"
        return ModuleTester(name, funcs, input_data)

    def __get_stcpython_tester(self):
        """
        :return: возвращает тестировщик для Си-функций, вызов которых осуществляется из кода Питона с использованием
                стандартного, низкоуровневого метода для языка Питон
        """
        # словарь анализируемых функций
        funcs = {
#            "normal_distr_density": cf_stcpython.normal_distr_density,
            "sin_degrees": cf_stcpython.sin_degrees,
            "solve_square_equation": cf_stcpython.solve_square_equation,
            "sum_of_infinite_series": cf_stcpython.sum_of_infinite_series,
            "factorial": cf_stcpython.factorial,
            "get_abs_dif_max_min": cf_stcpython.get_abs_dif_max_min,
            "get_first_longest_word": cf_stcpython.get_first_longest_word,
            "copy_strs": cf_stcpython.copy_strs,
            "compute_rpn": cf_stcpython.compute_rpn,
            "compute_fib": cf_stcpython.compute_fib,
            "get_max_len_incr_series": cf_stcpython.get_max_len_incr_series,
            "multipl_table": cf_stcpython.multipl_table,
            "tr_square": cf_stcpython.tr_square,
            "tr_contains_point": cf_stcpython.tr_contains_point,
            "get_substr_levlen": cf_stcpython.get_substr_levlen,
        }
        # данные для некоторых функций
        a = cf_stcpython.Point(0, 0)
        b = cf_stcpython.Point(5, 0)
        c = cf_stcpython.Point(0, 5)
        id = 1
        tr = cf_stcpython.Triangle(id, a, b, c)
        point = cf_stcpython.Point(-0.001, -0.001)
        input_data = {
            "tr": tr,
            "point": point,
        }
        # имя способа
        name = "stcpython"
        return ModuleTester(name, funcs, input_data)

    def __get_python_impl_tester(self):
        """
        :return: возвращает тестировщик для функций, написанных исключительно на Питоне без загрузки Си-функций
        """
        # словарь анализируемых функций
        funcs = {
            "sin_degrees": python_impl.sin_degrees,
            "solve_square_equation": python_impl.solve_square_equation,
            "sum_of_infinite_series": python_impl.sum_of_infinite_series,
            "factorial": python_impl.factorial,
            "get_abs_dif_max_min": python_impl.get_abs_dif_max_min,
            "get_first_longest_word": python_impl.get_first_longest_word,
            "copy_strs": python_impl.copy_strs,
            "compute_rpn": python_impl.compute_rpn,
            "compute_fib": python_impl.compute_fib,
            "get_max_len_incr_series": python_impl.get_max_len_incr_series,
            "multipl_table": python_impl.multipl_table,
            "tr_square": python_impl.tr_square,
            "tr_contains_point": python_impl.tr_contains_point,
            "get_substr_levlen": python_impl.get_substr_levlen,
        }
        # данные для некоторых функций
        a = python_impl.Point(0, 0)
        b = python_impl.Point(5, 0)
        c = python_impl.Point(0, 5)
        id = 1
        tr = python_impl.Triangle(id, a, b, c)
        point = python_impl.Point(-0.001, -0.001)
        input_data = {
            "tr": tr,
            "point": point,
        }
        # имя способа
        name = "pure_python"
        return ModuleTester(name, funcs, input_data)

    def __init__(self):
        """
        заполнение словаря тестировщиков
        """
        self.__testers_dict = {
            "ctypes": self.__get_ctypes_tester(),
            "cffi": self.__get_cffi_tester(),
            "stcpython": self.__get_stcpython_tester(),
            "python_impl": self.__get_python_impl_tester(),
        }

    def create_tester(self, name):
        """
        :param name: имя тестировщика
        :return: возвращает тестировщик по имени
        """
        return self.__testers_dict[name]

    def create_testers_container(self):
        """
        :return: контейнер тестировщиков
        """
        return TestersContainer(self.__testers_dict)

# кортеж для числа повторений
RepCounts = namedtuple("RepCounts",
    "lin branch cond num_factorial num_vector multi_matrix " \
    "multi_mtable strings files subrs recursive structs compl")

# кортеж для результатов тестов
TestResults = namedtuple("TestResults", "program time memory rep_count")


class AbstractTester(metaclass=abc.ABCMeta):
    """
    абстрактный класс для тестировщиков
    """
    @abc.abstractmethod
    def test(self, args):
        """
        тестирование
        :param args: аргументы для тестирования
        """
        raise NotImplementedError()

class TestersContainer(AbstractTester):
    """
    контейнер тестировщиков
    """
    def __init__(self, testers):
        """
        :param testers: словарь тестировщиков
        """
        self.__proc_count = 2 * mp.cpu_count()
        self.__testers = testers

    @property
    def testers(self):
        return self.__testers

    def test(self, args):
        """
        тестирование
        """
        results = {}
        count = 0
        for item in self.testers.values():
            res = item.test(args)
            results[res[0]] = res[1]
            count = len(res[1])
        return results, count


class ModuleTester(AbstractTester):
    """
    класс для тестировщиков функций
    """
    def __init__(self, method_name, funcs_for_testing, input_data=None):
        """
        :param method_name: имя метода
        :param funcs_for_testing: функции для тестирования
        """
        self.__funcs = funcs_for_testing
        self.__method_name = method_name
        self.__input_data = input_data

    def test(self, rep_counts):
        """
        анализ времени выполнения, памяти при выполнении заданных функций
        """
        test_results = []
        # линейные программы
        t, mem = self.linear_progs_test(rep_counts.lin)
        test_results.append(TestResults(
            "Линейная",
            time=t, memory=mem, rep_count=rep_counts.lin
        ))
        # ветвящиеся программы
        t, mem = self.branching_progs_test(rep_counts.branch)
        test_results.append(TestResults(
            "Ветвящаяся",
            time=t, memory=mem, rep_count=rep_counts.branch
        ))
        # циклы с условием
        t, mem = self.condition_cycles_test(rep_counts.cond)
        test_results.append(TestResults(
            "Цикл с условием",
            time=t, memory=mem, rep_count=rep_counts.cond
        ))
        # циклы с заданным числом итераций
        # Факториал
        t, mem = self.number_cycles_test(rep_counts.num_factorial)
        test_results.append(TestResults(
            "Цикл с заданным числом\nитераций (факториал)",
            time=t, memory=mem, rep_count=rep_counts.num_factorial
        ))
        # вектор
        size = 1000
        vec = array.array(
            "d",
            [random.randint(-size, size) for item in range(size)]
        )

        f = self.funcs["get_max_len_incr_series"]
        t, mem = self.get_test_results(
            "",
            lambda : f(vec, size),
            count_rep=rep_counts.num_vector#10000
        )
        test_results.append(TestResults(
            "Цикл с заданным числом\nитераций (вектор)",
            time=t, memory=mem, rep_count=rep_counts.num_vector
        ))

        # многоуровневые циклы
        # матрица
        t, mem = self.multi_cycles_test(rep_counts.multi_matrix)
        test_results.append(TestResults(
            "Многоступенчатый цикл\n(матрица)",
            time=t, memory=mem, rep_count=rep_counts.multi_matrix
        ))

        # таблица умножения
        f = self.funcs["multipl_table"]
        t, mem = self.get_test_results(
            "",
            lambda : f(),
            count_rep=rep_counts.multi_mtable#10000
        )
        test_results.append(TestResults(
            "Многоступенчатый цикл\n(таблица умножения)",
            time=t, memory=mem, rep_count=rep_counts.multi_mtable
        ))

        # работа со строками
        t, mem = self.strings_test(rep_counts.strings)
        test_results.append(TestResults(
            "Обработка строк",
            time=t, memory=mem, rep_count=rep_counts.strings
        ))
        # работа с файлами
        t, mem = self.files_test(rep_counts.files)
        test_results.append(TestResults(
            "Работа с файлами",
            time=t, memory=mem, rep_count=rep_counts.files
        ))
        # подпрограммы
        t, mem = self.subroutines_test(rep_counts.subrs)
        test_results.append(TestResults(
            "Подпрограммы",
            time=t, memory=mem, rep_count=rep_counts.subrs
        ))
        t, mem = self.subroutines_test(rep_counts.recursive)
        test_results.append(
            TestResults(
                "Рекурсия",
                time=t, memory=mem, rep_count=rep_counts.recursive
            )
        )
        # структуры
        self.structs_test(rep_counts, test_results)

        # комплексная задача
        self.complex_task_test(rep_counts, test_results)

        return (self.method_name, test_results)

    @property
    def method_name(self):
        """
        :return: имя способа
        """
        return self.__method_name

    @property
    def funcs(self):
        """
        :return: словарь анализируемых функций
        """
        return self.__funcs

    @property
    def input_data(self):
        """
        :return: входные данные для некоторых функций
        """
        return self.__input_data

#   @funcs.setter
#   def funcs(self, value):
#       self.__funcs = value

    def get_test_results(self, description, func, count_rep=1000):
        """
        :param description: описание
        :param func: анализируемая функция
        :param count_rep: число повторений
        :return: время работы функции func за count_rep повторений
        """
        memory = 0
        duration = 0
        result = None
        for i in range(count_rep):
            begin = time.perf_counter()
            result = func()
            end = time.perf_counter()
            duration += end - begin
        return duration, memory

    def structs_test(self, reps, test_results):
        """
        тест структур
        """
        rep_count = reps.structs
        tr = self.input_data["tr"]
        point = self.input_data["point"]

        # тест площади треугольника
        f = self.funcs["tr_square"]
        t, mem = self.get_test_results(
            "",
            lambda : f(tr),
            count_rep=rep_count
        )
        test_results.append(
            TestResults(
                "Структуры\n(площадь треугольника)",
                time=t, memory=mem, rep_count=rep_count
            )
        )

        # тест определения содержания точки внутри треугольника
        f = self.funcs["tr_contains_point"]
        t, mem = self.get_test_results(
            "",
            lambda : f(tr, point),
            count_rep=rep_count
        )
        test_results.append(
            TestResults(
                "Структуры (содержание\nточки в треугольнике)",
                time=t, memory=mem, rep_count=rep_count
            )
        )

    def complex_task_test(self, reps, test_results):
        """
        тест комплексной задачи
        """
        substr = "long string"
        string = "This is very long string....."
        rep_count = reps.compl
        f = self.funcs["get_substr_levlen"]
        t, mem = self.get_test_results(
            "",
            lambda : f(substr, string),
            count_rep=rep_count
        )
        test_results.append(
            TestResults(
                "Комплексная задача",
                time=t, memory=mem, rep_count=rep_count
            )
        )

    def linear_progs_test(self, rep_count):
        """
        тест линейной программы
        """
        f = self.funcs["sin_degrees"]
        x = math.pi
        return self.get_test_results(
            "sin({:.5})\n".format(x),
            lambda : f(x),
            count_rep=rep_count#100000
        )

    def branching_progs_test(self, count_rep):
        """
        тест ветвящейся программы
        """
        a = 1
        b = 2
        c = 1
        f = self.funcs["solve_square_equation"]
        return self.get_test_results(
            "{}*x*x + {}*x + {} = 0\n".format(a, b, c),
            lambda : f(a, b, c),
            count_rep=count_rep#1000000
        )

    def  condition_cycles_test(self, count_rep):
        """
        тест программы, использующей цикл с условием
        """
        epsilon = 1e-10
        x = 5
        f = self.funcs["sum_of_infinite_series"]
        return self.get_test_results(
            "sum of x + x/2! + x/3! + ... with epsilon={}, x={}\n".format(epsilon, x),
            lambda : f(x, epsilon),
            count_rep=count_rep#1000000
        )

    def number_cycles_test(self, count_rep):
        """
        тест программы, использующей цикл с фиксированным числом итераций
        """
        n = 1000
        f = self.funcs["factorial"]
        return self.get_test_results(
            "{}!\n".format(n),
            lambda : f(n),
            count_rep=count_rep#10000
        )

    def multi_cycles_test(self, count_rep, choose=0):
        """
        тест программы, использующей вложенные циклы
        """
        nrows = 300
        ncolumns = 360
        size = nrows * ncolumns
        if choose != 0 and self.method_name == "pure_python":
            matr = [[random.randint(-size, size) for j in range(ncolumns)] for i in range(nrows)]
        else:
            matr = array.array(
                "d", [random.randint(-size, size) for item in range(size)]
            )

        f = self.funcs["get_abs_dif_max_min"]
        return self.get_test_results(
            "find absolute value of maximum and minimum in matrix\n",
            lambda : f(matr, nrows, ncolumns),
            count_rep=count_rep#200
        )

    def strings_test(self, count_rep):
        """
        тест программы, обрабатывающей  строки
        """
        input_str = "- This, a sample string\t.555"
        f = self.funcs["get_first_longest_word"]
        return self.get_test_results(
            'get the first longest word of string "{}"\n'.format(input_str),
            lambda :  f(input_str),
            count_rep=count_rep#1000000
        )

    def files_test(self, count_rep):
        """
        тест программы для работы с файлами
        """
        input = "input.txt"
        output = self.method_name + "_output.txt"
        start = 5
        end = 25
        f = self.funcs["copy_strs"]
        try:
            return self.get_test_results(
                "copy strings with numbers in range [{}, {}] from file {} to file {}\n".format(start, end, input, output),
                lambda : f(input, output, start, end),
                count_rep=count_rep#10000
            )
        except Exception as error:
            return str(error), str(error)

    def subroutines_test(self, count_rep):
        """
        тест программы, использующей подпрограммы
        """
        rpn_str = "50 5 * 10 + 26 / 3 ^"
        try:
            f = self.funcs["compute_rpn"]
            return self.get_test_results(
                "computation of expression in reverse polish notation \"{}\"\n".format(rpn_str),
                lambda : f(rpn_str),
                count_rep=count_rep#100000
            )
        except Exception as error:
            return str(error), str(error)

    def recursive_test(self, count_rep):
        """
        тест рекурсивной функции
        """
        number = 30
        f = self.funcs["compute_fib"]
        return self.get_test_results(
                "computation of fibonacci",
                lambda : f(number),
                count_rep=count_rep#100000
            )
