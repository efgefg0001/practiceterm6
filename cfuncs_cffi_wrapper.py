"""
Способ вывзова Си-функций из Python, используя библиотеку cffi
"""

import sys
import os
import array
from cffi import FFI

# создание экземпляра класса FFI
ffi = FFI()

# установка сигнатур (заголовков) используемых Си-функций,
# определения структур и констант
ffi.cdef("""
// linear programs
  double normal_distr_density(double x, double mu, double sigma, unsigned int *memory);
  double squared_x(double x, unsigned int *memory);
  double sin_degrees(double degrees, unsigned int *memory);
  int func(int x);

// branching programs
  const char *solve_square_equation(double a, double b, double c, unsigned int *memory);

// cycles with conditions
  double sum_of_infinite_series(double x, double epsilon, unsigned int *memory);

// cycles with number of iterations
double factorial(unsigned int number, unsigned int *memory);
unsigned int get_max_len_incr_series(double *arr, unsigned int count, unsigned int *memory);

// multi-cycles
double get_abs_dif_max_min(double *matr, int nrows, int ncolumns, unsigned int *memory);
void multipl_table(unsigned int *memory);

// strings
const char *get_first_longest_word(char *str, unsigned int *memory);

// files
const int BUF_SIZE = 1001;
typedef enum {ERROR_OPEN_READ=1, ERROR_OPEN_WRITE} file_errors;
int copy_strs(const char *input, const char *output, unsigned int start, unsigned int end, unsigned int *memory);

// subroutines
const int MAX_D = 256;
double compute_rpn(char *str, unsigned int *memory);

// recursive
int compute_fib(int number);

// structs
typedef struct Point {
	double x, y;
} Point;

double distance(Point *p1, Point *p2);

typedef struct Triangle {
	int id;
	Point *a, *b, *c;
} Triangle;

double tr_square(Triangle *tr);
int tr_contains_point(Triangle *tr, Point *point);

// complex task
typedef struct SubStrLevLen {
	int substr;
	int levlen;
} SubStrLevLen;
SubStrLevLen *get_substr_levlen(const char *substr, const char *str);
""")

# путь к загружаемой библиотеке Си-функций
path = "./CFunctions/Debug/CFunctionsDll.dll"
# загрузка этой библиотеки
cffi_lib = ffi.dlopen(path)
include_convert = False

if include_convert:
    def tr_square(tr):
        """
        cffi-обёртка на tr_square
        """
        a = ffi.new("Point *")
        a.x, a.y = tr.a.x, tr.a.y
        b = ffi.new("Point *")
        b.x, b.y = tr.b.x, tr.b.y
        c = ffi.new("Point *")
        c.x, c.y = tr.c.x, tr.c.y
        tr_cast = ffi.new("Triangle *")
        tr_cast.id, tr_cast.a, tr_cast.b, tr_cast.c = (tr.id, a, b, c)
        result = cffi_lib.tr_square(tr_cast)
        return (result, 0)

    def tr_contains_point(tr, point):
        """
        cffi-обёртка над tr_contains_point
        """
        a = ffi.new("Point *")
        a.x, a.y = tr.a.x, tr.a.y
        b = ffi.new("Point *")
        b.x, b.y = tr.b.x, tr.b.y
        c = ffi.new("Point *")
        c.x, c.y = tr.c.x, tr.c.y
        tr_cast = ffi.new("Triangle *")
        tr_cast.id, tr_cast.a, tr_cast.b, tr_cast.c = (tr.id, a, b, c)
        point_cast = ffi.new("Point *")
        point_cast.x, point_cast.y = (-0.001, -0.001)
        result = cffi_lib.tr_contains_point(tr_cast, point_cast)
        return (result, 0)
else:
    def tr_square(tr):
        """
        cffi-обёртка над tr_square
        """
        result = cffi_lib.tr_square(tr)
        return (result, 0)


    def tr_contains_point(tr, point):
        """
        cffi-обёртка над tr_contains_point
        """
        result = cffi_lib.tr_contains_point(tr, point)
        return (result, 0)


def sin_degrees(degrees):
    """
    cffi-обёртка над sin_degrees
    """
    # выделение память под указатель
    memory = ffi.new("unsigned int *")
    # вызов Си-функции
    result = cffi_lib.sin_degrees(degrees, memory)
    return (result, memory[0])

def solve_square_equation(a, b, c):
    """
    cffi-обёртка над solve_square_equation
    """
    # выделение памяти поду указатель
    memory = ffi.new("unsigned int *")
    # вызов Си-функции
    result = ffi.string(cffi_lib.solve_square_equation(a, b, c, memory))
    return (result, memory[0])


def sum_of_infinite_series(x, epsilon):
    """
    cffi-обёртка над sum_of_infinite_series
    """
    # выделение памяти под указатель
    memory = ffi.new("unsigned int *")
    # вызов Си-функции
    result = cffi_lib.sum_of_infinite_series(x, epsilon, memory)
    return (result, memory[0])


def factorial(number):
    """
    cffi-обёртка над factorial
    """
    # выделение памяти под указатель
    memory = ffi.new("unsigned int *")
    # вызов Си-функции
    result = cffi_lib.factorial(number, memory)
    return (result, memory[0])


def get_abs_dif_max_min(matr, nrows, ncolumns):
    """
    cffi-обёртка над get_abs_dif_max_min
    """
    # выделение памяти под указатель
    memory = ffi.new("unsigned int *")
    # получение указателя на начало матрицы и размера
    addr, size = matr.buffer_info()
    # преобразование указателя на начало матрицы к double *
    double_arr = ffi.cast("double *", addr)
    # вызов Си-функции
    result = cffi_lib.get_abs_dif_max_min(double_arr, nrows, ncolumns, memory)
    return (result, memory[0])


def get_first_longest_word(in_str):
    """
    cffi-обёртка над get_first_longest_word
    """
    # выделение памяти под указатель
    memory = ffi.new("unsigned int *")
    # преобразование входной строки в байты
    input_str = ffi.new("char[]", str.encode(in_str))
    # вызов Си-функции, обратное преобразование строки
    result = bytes.decode(ffi.string(cffi_lib.get_first_longest_word(input_str, memory)))
    return (result, memory[0])


def copy_strs(input, output, start, end):
    """
    cffi-обёртка над copy_strs
    """
    # выделение памяти под указатель
    memory = ffi.new("unsigned int *")
    # преобразование имени входного файла в байты
    input_bytes = ffi.new("char []", str.encode(input))
    # преобразование имени выходного файла в байты
    output_bytes = ffi.new("char []", str.encode(output))
    # вызов Си-функции
    result = cffi_lib.copy_strs(input_bytes, output_bytes, start, end, memory)
    return (result, memory[0])


def compute_rpn(in_str):
    """
    cffi-обёртка над compute_rpn
    """
    # выделение памяти под указатель
    memory = ffi.new("unsigned int *")
    # преобразование входной строки в байты
    in_str_bytes = ffi.new("char []", str.encode(in_str))
    # вызов Си-функции
    result = cffi_lib.compute_rpn(in_str_bytes, memory)
    return (result, memory[0])


def compute_fib(number):
    """
    cffi-обёртка над compute_fib
    """
    # вызов Си-функции
    result = cffi_lib.compute_fib(number)
    return (result, 0)


def get_max_len_incr_series(arr, count):
    """
    cffi-обёртка над get_max_len_incr_series
    """
    # выделение памяти под указатель
    memory = ffi.new("unsigned int *")
    # получение указателья на содержимое вектора и размера
    addr, size = arr.buffer_info()
    # преобразование полученного указателя к double *
    double_arr = ffi.cast("double *", addr)
    # вызов Си-функции
    result = cffi_lib.get_max_len_incr_series(double_arr, count, memory)
    return (result, memory[0])


def multipl_table():
    """
    cffi-обёртка над multipl_table
    """
    # выделение памяти под указатель
    memory = ffi.new("unsigned int *")
    # вызов Си-функции
    cffi_lib.multipl_table(memory)
    return (None, memory[0])


def get_substr_levlen(substr, string):
    """
    cffi-обёртка над get_substr_levlen
    """
    # выделение памяти под указатель на структуру
    result = ffi.new("SubStrLevLen *")
    # преобразование подстроки в байты
    substr_bytes= ffi.new("char []", str.encode(substr))
    # преобразование строки в байты
    string_bytes = ffi.new("char []", str.encode(string))
    # вызов Си-функции
    result = cffi_lib.get_substr_levlen(substr_bytes, string_bytes)
    return result.substr, result.levlen
