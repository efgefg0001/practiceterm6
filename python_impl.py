"""
Реализация на чистом Python 3 анализируемых подпрограмм.
"""

import math
import array
import random
import time


def sin_degrees(degrees):
    """
    :param degrees: угол в градусах
    :return: синус угла в градусах
    """
    radians = degrees * math.pi / 180
    sin_val = math.sin(radians)
    return (sin_val, None)


# точность
EPS = 1e-10


def solve_square_equation(a, b, c):
    """
    :param a: коэффициент при x*x
    :param b: коэффициент при x
    :param c: свободный член
    :return: решение квадратного уравнения
    """
    eps = EPS
    str = ""
    if abs(a) < eps:
        if abs(b) < eps:
            if abs(c) < eps:
                str = "infinity"
            else:
                str = "no solutions"
        else:
            str = "x={}".format(-c/b)
    else:
        D = b*b - 4*a*c
        if(D > 0):
            str = "x1 = {}, x2 = {}".format((-b + math.sqrt(D))/(2*a), (-b - math.sqrt(D))/(2*a))
        elif(D < 0):
            str = "no solutions"
        else:
            str = "x1 = x2 = {}".format(-b/(2*a))
    return (str, None)


def sum_of_infinite_series(x, epsilon):
    """
    :param x: аргумент
    :param epsilon: точность
    :return: сумма бесконечного ряда x + x/2! + x/3! + x/4! + ... с точностью epsilon
    """
    i = 1
    cur = x
    i += 1
    next = cur / i
    sum = cur
    while abs(next - cur) >= epsilon:
        cur = next
        sum += cur
        i += 1
        next /= i
    return (sum, None)


def factorial(number):
    """
    :param number: натуральное число
    :return: number!
    """
    fact = 1.0
    max_num = number + 1
    for i in range(1, max_num):
        fact *= i
    return (fact, None)


def get_abs_dif_max_min(matr, nrows, ncolumns):
    """
    :param matr: матрица
    :param nrows: число строк матрицы
    :param ncolumns: число столбцов матрицы
    :return: модуль разности минимума и максимума в матрице
    """
    max = min = matr[0]
#    max = min = matr[0][0]
    for i in range(nrows):
        incolumns = i * ncolumns
        for j in range(ncolumns):
            cur_item = matr[incolumns + j]
#            cur_item = matr[i][j]
            if cur_item > max:
                max = cur_item
            elif cur_item < min:
                min = cur_item
    result = abs(max - min)
    return (result, None)

def get_abs_dif_max_min_ij(matr, nrows, ncolumns):
    """
    :param matr: матрица
    :param nrows: число строк матрицы
    :param ncolumns: число столбцов матрицы
    :return: модуль разности минимума и максимума в матрице
    """
#    max = min = matr[0]
    max = min = matr[0][0]
    for i in range(nrows):
#        incolumns = i * ncolumns
        for j in range(ncolumns):
#            cur_item = matr[incolumns + j]
            cur_item = matr[i][j]
            if cur_item > max:
                max = cur_item
            elif cur_item < min:
                min = cur_item
    result = abs(max - min)
    return (result, None)

def get_times(count):
    """
    сравнение матриц на основе одномерного array и списка списков
    :param count: число повторений
    """
    nrows = 300
    ncolumns = 360
    size = nrows * ncolumns
    matr2 = [[random.randint(-size, size) for j in range(ncolumns)] for i in range(nrows)]
    matr1 = array.array(
       "d", [random.randint(-size, size) for item in range(size)]
    )
    dur1 = dur2 = 0
    res1 = None
    # одномерный array
    for i in range(count):
        beg = time.perf_counter()
        res1 = get_abs_dif_max_min(matr1, nrows, ncolumns)
        end = time.perf_counter()
        dur1 += end - beg
    # список списков
    for i in range(count):
        beg = time.perf_counter()
        res1 = get_abs_dif_max_min_ij(matr2, nrows, ncolumns)
        end = time.perf_counter()
        dur2 += end - beg
    print("1-dim = {}, 2-dim = {}".format(dur1, dur2))


def get_first_longest_word(str):
    """
    :param str: входная строка
    :return: первое самое длинное слово во входной строке
    """
    whitespace = " \t\r\n\v\f"
    words = str.split(whitespace)
    max_len_word = None
    max_len = 0
    for word in words:
        word_len = len(word)
        if word_len > max_len:
            max_len = word_len
            max_len_word = word
    return (max_len_word, None)


def copy_strs(input, output, start, end):
    """
    копирует строки с номерами от start до end из файла input в файл output
    :param input: имя входного файла
    :param output: имя выходного файла
    :param start: номер начальной строки для интервала копирования
    :param end: номер конечной строки для интервала копирования
    """
    with open(input, "r") as fin, \
         open(output, "w") as fout:
        i = 1
        for file_string in fin:
            if start <= i  <= end:
                fout.write(file_string)
            i += 1
    return (0, None)


def op_pow(stack):
    """
    возведение в степень
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a ** b )


def op_mul(stack):
    """
    умножение
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a * b )


def op_div(stack):
    """
    деление
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a / b )


def op_add(stack):
    """
    сложение
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a + b )


def op_sub(stack):
    """
    вычитание
    :param stack: стек, использующийся при вычислении значения выражения
    """
    b = stack.pop(); a = stack.pop()
    stack.append( a - b )


def op_num(stack, num):
    """
    добавление числа num в стек
    :param stack: стек, использующийся при вычислении значения выражения
    :param num: число
    """
    stack.append( num )


def get_input(inp = None):
    """
    :param inp: выражение в формате обратной польской записи
    :return: список токенов, на которые разбивается входная строка inp
    """
    if inp is None:
        inp = input('expression: ')
    tokens = inp.strip().split()
    return tokens


def compute_rpn(rpn_str):
    """
    :param rpn_str: выражение в обратной польской записи в виде строки
    :return: значение выражения в обратной польской записи rpn_str
    """
    tokens = get_input(rpn_str)
    result = rpn_calc(tokens)
    return result


def rpn_calc(tokens):
    """
    :param tokens: токены, на которые разбивается выражение в обратной польской записи
    :return: значение выражения в обратной польской записи
    """
    stack = []
    # операции
    ops = {
        '^': op_pow,
        '*': op_mul,
        '/': op_div,
        '+': op_add,
        '-': op_sub,
    }
    # выполнение операций
    for token in tokens:
        if token in ops:
            ops[token](stack)
        else:
            op_num(stack, eval(token))
    return (stack[0], None)


def compute_fib(number):
    """
    :param number: номер числа Фибоначчи
    :return: число Фибоначчи по его номеру
    """
    if number == 0:
        return 0
    elif number == 1:
        return 1
    else:
        return compute_fib(number - 2) + compute_fib(number - 1)


def get_max_len_incr_series(arr, count):
    """
    :param arr: исходный массив
    :param count: количество элементов в массиве arr
    :return: максимальную длину последовательности подряд идущих возрастающих
        элементов массива. Если элементы равны, то начинается новая последовательность.
    """
    len_incr_series = 1
    max_len_incr_series = len_incr_series
    for i in range(1, count):
        if arr[i-1] < arr[i]:
            # возрастание
            len_incr_series += 1
        elif len_incr_series > max_len_incr_series:
            # невозрастание
            max_len_incr_series = len_incr_series
            len_incr_series = 1
    # обработка последней последовательности
    if len_incr_series > max_len_incr_series:
        max_len_incr_series = len_incr_series
    return max_len_incr_series


def multipl_table():
    """
    печатает таблицу умножения
    """
    nrows = 10
    ncolumns = 10
    for i in range(1, nrows):
        for j in range(1, ncolumns):
            print("{:4}".format(i * j), end=" ")
        print()

class Point(object):
    """
    точка на плоскости
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Triangle(object):
    """
    треугольник на плоскости
    """
    def __init__(self, id, point_a, point_b, point_c):
        self.id = id
        self.a = point_a
        self.b = point_b
        self.c = point_c


def distance(p1, p2):
    """
    :param p1: первая точка
    :param p2: вторая точка
    :return: расстояние между первой и второй точками
    """
    dx = p2.x - p1.x
    dy = p2.y - p1.y
    dist = math.sqrt(dx**2 + dy**2)
    return dist


def tr_square(tr):
    """
    :param tr: треугольник
    :return: площадь треугольника
    """
    ab = distance(tr.a, tr.b)
    bc = distance(tr.b, tr.c)
    ca = distance(tr.c, tr.a)
    p = (ab + bc + ca)/2
    s = math.sqrt(p * (p - ab) * (p - bc) * (p - ca))
    return (s, None)


def sign(p1, p2, p3):
    """
    :param p1: координаты точки
    :param p2: координаты первой точки для задания прямой
    :param p3: координаты второй точки для задания прямой
    :return: положительное значение, если точки лежит слева от прямой,
        отрицательное, если точка лежит справа от прямой
    """
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y)


def tr_contains_point(tr, point):
    """
    :param tr: треугольник
    :param point: точка
    :return: лежит ли точка внутри треугольника
    """
    b1 = sign(point, tr.a, tr.b) < 0
    b2 = sign(point, tr.b, tr.c) < 0
    b3 = sign(point, tr.c, tr.a) < 0
    contains = (b1 == b2) and (b2 == b3)
    return (contains, None)


def get_levlen(s, t):
    """
    :param s: строка 1
    :param t: строка 2
    :return:  расстояние Левенштейна между строкой 1 и строкой 2 по алгоритму
        Вагнера-Фишера
    """
    # длины строк
    n = len(s)
    m = len(t)
    # размер матрицы
    nrows = n + 1
    ncols = m + 1
    size = nrows * ncols
    d = array.array("i", [0 for i in range(size)])
    # вырожденные случаи
    if n == 0:
        return m
    if m == 0:
        return n
    # заполнений нулевого столбца и нулевой строки матрицы
    for i in range(nrows):
        d[i*ncols] = i
    for j in range(ncols):
        d[j] = j
    # расчёт матрицы D
    for i in range(1, nrows):
        # сохраняем значения для ускорения вычислений
        incols = i*ncols
        incolsm1 = incols - 1
        im1ncols = (i-1)*ncols
        im1ncolsm1 = im1ncols - 1
        for j in range(1, ncols):
            # если элементы строк равны, то стоимость ноль
            if t[j - 1] == s[i - 1]:
                cost = 0
            # иначе 1
            else:
                cost = 1
            # считаем, минимальную стоимость перехода
            d[incols + j] = min(
				min(d[im1ncols + j] + 1, d[incolsm1 + j] + 1),
                d[im1ncolsm1 + j] + cost
			)
    # возвращаем полученное расстояние Левенштейна
    return d[n*ncols + m]


def get_fail_arr(substr):
    """
    :param substr: подстрока
    :return: массив, содержащий переходы по несовпадению
    """
    substr_len = len(substr)
    fail = array.array("i")
    fail.append(-1)
    # проходим по подстроке
    for i in range(1, substr_len):
        temp = fail[i - 1]
        # производим вычисления
        while temp > 0 and substr[temp] != substr[i - 1]:
            temp = fail[temp]
        fail.append(temp + 1)
    return fail


def get_substr(substr, string):
    """
    :param substr: подстрока
    :param string: строка
    :return: индекса элемента, начиная с которого подстрока начинает входить в
        строку, рассчитанный по алгоритму Кнута-Морриса-Пратта
    """
    substrLoc = 0
    strLoc = substrLoc
    # получения вектора переходов по несовпадению
    fail = get_fail_arr(substr)
    str_len = len(string)
    substr_len = len(substr)
    # находим необходимый индекс
    while strLoc < str_len and substrLoc < substr_len:
        if substrLoc == -1 or string[strLoc] == substr[substrLoc]:
            strLoc += 1
            substrLoc += 1
        else:
            substrLoc = fail[substrLoc]

    return  strLoc - substrLoc if substrLoc >= substr_len else -1


def get_substr_levlen(substr, string):
    """
    :param substr: подстрока
    :param string: строка
    :return: позицию начала вхождения подстроки в строке по алгоритму Кнута-Морриса-Пратта,
        расстояние Левенштейна между этими строками по алг. Вагнера — Фишера
    """
    substr_ind = get_substr(substr, string)
    levlen = get_levlen(substr, string)
    result = (substr_ind, levlen)
    return result
